#include <stdio.h>
#include <string.h>

#include <node_api.h>

#include <alleycat.h>

#include "addon-util.h"
#include "drupn-addon.h"
#include "drup.h"
#include "drup-util.h"

#define sizeof_ary(xs) (sizeof (xs) / sizeof (xs[0]))

static bool cleaned_up = false;

static const char *get_jname (napi_env env, napi_callback_info info) {
  struct finfo *finfo;
  ac_ncall_ok_rnull (env, get_data, (env, info, (void **) &finfo));
  return finfo->jname;
}

static bool check_cleaned_up_throw (napi_env env) {
  if (cleaned_up) throw_rf (env, "Invalid state: already called cleanup ()?");
  return true;
}

static bool get_controller_type (napi_env env, napi_value ntype, controller_type_t *type) {
  char str[11];
  bool truncated;
  ac_ncall_ok_rfalse (env, get_value_string_latin1_check_truncated, (env, ntype, 11, str, &truncated));
  if (!strcmp (str, "ds2482-800")) return (*type = DS2482_800, true);
  if (!strcmp (str, "ds2482-100")) return (*type = DS2482_100, true);
  if (!strcmp (str, "ds2477"    )) return (*type = DS2477,     true);
  throw_rf (env, "Invalid controller type %s%s", str, truncated ? "…" : "");
}

static bool get_authenticator_type (napi_env env, napi_value ntype, authenticator_type_t *type) {
  char str[7];
  bool truncated;
  ac_ncall_ok_rfalse (env, get_value_string_latin1_check_truncated, (env, ntype, 7, str, &truncated));
  if (!strcmp (str, "ds1961")) return (*type = DS1961, true);
  if (!strcmp (str, "ds1964")) return (*type = DS1964, true);
  throw_rf (env, "Invalid authenticator type %s%s", str, truncated ? "…" : "");
}

static bool get_device (napi_env env, napi_value ndevice, size_t n, char *ret) {
  bool truncated;
  if (n < 2) ac_piep_rf;
  ac_ncall_ok_rfalse (env, get_value_string_latin1_check_truncated, (env, ndevice, n + 1, ret, &truncated));
  if (truncated) throw_rf (env, "%s (): device too long (max %zd chars)", __func__, n - 1);
  return true;
}

static drup_t wrap_drup_mk (napi_env env, const char *jname, napi_value nctrls, napi_value nauths, napi_value nverbose, napi_value nmock) {
  uint32_t nc, na;
  ac_ncall_ok_rnull (env, expect_array_get_length, (env, nctrls, &nc, "%s (): ", jname));
  ac_ncall_ok_rnull (env, expect_array_get_length, (env, nauths, &na, "%s (): ", jname));
  if (nc != (uint8_t) nc) throw_rn (env, "%s (): too many controllers (max is 255, got %u)", jname, nc);
  if (na != (uint8_t) na) throw_rn (env, "%s (): too many authenticators (max is 255, got %u)", jname, na);
  ac_try_calloc_rnull (drup_controller_t *, ctrls, nc);
  ac_try_calloc_rnull (drup_authenticator_t *, auths, na);
  bool mock;
  ac_ncall_nstatus_rnull_msg (env, napi_get_value_bool, (env, nmock, &mock), "Expected boolean");
  for (uint8_t i = 0; i < nc; i++) {
    napi_value xs, ntype, ndevice, naddr;
    ac_ncall_nstatus_rnull (env, napi_get_element, (env, nctrls, i, &xs));
    uint32_t n;
    ac_ncall_ok_rnull (env, expect_array_get_length, (env, xs, &n, "%s (): ", jname));
    if (n != 3) throw_rn (env, "%s (): invalid usage (check controllers)", jname);
    ac_ncall_nstatus_rnull (env, napi_get_element, (env, xs, 0, &ntype));
    ac_ncall_nstatus_rnull (env, napi_get_element, (env, xs, 1, &ndevice));
    ac_ncall_nstatus_rnull (env, napi_get_element, (env, xs, 2, &naddr));
    controller_type_t type;
    // --- just an arbitary limit for the pathname of the device
    char device[51];
    uint8_t addr;
    ac_ncall_ok_rfalse (env, get_device, (env, ndevice, 51, device));
    ac_ncall_ok_rnull (env, get_controller_type, (env, ntype, &type));
    ac_ncall_ok_rnull (env, get_value_uint8, (env, naddr, &addr, "%s (): invalid addr: ", jname));

    drup_controller_t ctrl = drup_controller_mk (type, device, addr, mock);
    if (ctrl == NULL) ac_piep_rn;
    ctrls[i] = ctrl;
  }

  for (uint8_t i = 0; i < na; i++) {
    napi_value ntype;
    ac_ncall_nstatus_rnull (env, napi_get_element, (env, nauths, i, &ntype));
    authenticator_type_t type;
    ac_ncall_ok_rnull (env, get_authenticator_type, (env, ntype, &type));
    drup_authenticator_t auth = drup_authenticator_mk (type);
    if (auth == NULL) ac_piep_rn;
    auths[i] = auth;
  }
  bool verbose;
  ac_ncall_nstatus_rnull_msg (env, napi_get_value_bool, (env, nverbose, &verbose), "Expected boolean");
  return drup_mk (ctrls, nc, auths, na, verbose, mock);
}

static drup_t init (napi_env env, napi_callback_info info) {
  const char *jname = get_jname (env, info);
  if (jname == NULL) ac_piep_rn;
  napi_value argv[2] = {0};
  size_t good_sizes[] = { 1, 2, };
  size_t good_sizes_n = sizeof_ary (good_sizes);
  size_t argc = 2;
  ac_ncall_ok_rnull (env, get_args_var, (env, info, good_sizes_n, good_sizes, &argc, argv, "%s (): ", jname));
  napi_value tbl_config = argv[0];
  napi_value tbl_opts = (argc == 2) ? argv[1] : NULL;
  napi_value nmock;
  if (tbl_opts != NULL) {
    uint8_t expected_num_keys = 0;
    bool undefined;
    ac_ncall_ok_rnull (env, get_table_prop, (env, tbl_opts, "mock", &nmock, &undefined));
    if (undefined)
      ac_ncall_nstatus_rnull (env, napi_get_boolean, (env, false, &nmock));
    else expected_num_keys += 1;
    uint32_t n;
    ac_ncall_nstatus_rnull (env, table_get_num_entries, (env, tbl_opts, &n));
    if (n != expected_num_keys) throw_rf (env, "Unrecognized keys in table");
  }
  napi_value ncontrollers;
  napi_value nauthenticators;
  napi_value nverbose;
  {
    uint8_t expected_num_keys = 2;
    ac_ncall_ok_rnull (env, get_table_prop_strict, (env, tbl_config, "controllers", &ncontrollers, "%s (): ", jname));
    ac_ncall_ok_rnull (env, get_table_prop_strict, (env, tbl_config, "authenticators", &nauthenticators, "%s (): ", jname));
    ac_ncall_nstatus_rnull (env, napi_get_named_property, (env, tbl_config, "verbose", &nverbose));
    bool undefined;
    ac_ncall_ok_rnull (env, is_undefined, (env, nverbose, &undefined));
    if (undefined)
      ac_ncall_nstatus_rnull (env, napi_get_boolean, (env, false, &nverbose));
    else expected_num_keys += 1;
    ac_ncall_ok_rnull (env, table_expect_num_entries_throw, (env, tbl_config, expected_num_keys, "%s (): ", jname));
  }
  return wrap_drup_mk (env, jname, ncontrollers, nauthenticators, nverbose, nmock);
}

static bool get_device_id (napi_env env, const char *jname, napi_value ndevice_id, uint8_t *device_id) {
  unsigned char str[17];
  size_t s;
  ac_ncall_nstatus_rfalse (env, string_length_latin1, (env, ndevice_id, &s));
  if (s != 16) throw_rf (env, "%s (): expected device id to be exactly 16 chars long", jname);
  ac_ncall_ok_rfalse (env, get_value_string_latin1, (env, ndevice_id, 17, (char *) str));
  drup_hex_to_uint8 (str, device_id);
  return true;
}

static bool get_overdrive (napi_env env, napi_value noverdrive, overdrive_type_t *ret) {
  char str[10];
  bool truncated;
  ac_ncall_ok_rfalse (env, get_value_string_latin1_check_truncated, (env, noverdrive, 10, str, &truncated));
  if (!strcmp (str, "auto")) return (*ret = OVERDRIVE_AUTO, true);
  if (!strcmp (str, "force-off")) return (*ret = OVERDRIVE_FORCE_OFF, true);
  if (!strcmp (str, "mostly-on")) return (*ret = OVERDRIVE_MOSTLY_ON, true);
  throw_rf (env, "Invalid value for overdrive: %s%s", str, truncated ? "…" : "");
  return true;
}

static bool get_active_pullup (napi_env env, napi_value nactive_pullup, active_pullup_type_t *ret) {
  char str[10];
  bool truncated;
  ac_ncall_ok_rfalse (env, get_value_string_latin1_check_truncated, (env, nactive_pullup, 10, str, &truncated));
  if (!strcmp (str, "auto-new")) return (*ret = APU_AUTO_NEW, true);
  if (!strcmp (str, "auto-old")) return (*ret = APU_AUTO_OLD, true);
  if (!strcmp (str, "force-off")) return (*ret = APU_FORCE_OFF, true);
  if (!strcmp (str, "force-on")) return (*ret = APU_FORCE_ON, true);
  throw_rf (env, "Invalid value for active pullup: %s%s", str, truncated ? "…" : "");
  return true;
}

static bool get_network_type (napi_env env, napi_value nnetwork_type, network_type_t *ret) {
  char str[12];
  bool truncated;
  ac_ncall_ok_rfalse (env, get_value_string_latin1_check_truncated, (env, nnetwork_type, 12, str, &truncated));
  if (!strcmp (str, "single-drop")) return (*ret = SINGLE_DROP, true);
  if (!strcmp (str, "multi-drop")) return (*ret = MULTI_DROP, true);
  throw_rf (env, "Invalid value for network type: %s%s", str, truncated ? "…" : "");
  return true;
}

static bool get_ow_cfg (napi_env env, napi_value ntbl, const char *jname, struct drup_bus_user_config *ow_cfg) {
  napi_value noverdrive;
  napi_value nactive_pullup;
  napi_value nnetwork_type;
  ac_ncall_ok_rfalse (env, table_expect_num_entries_throw, (env, ntbl, 3, "%s (): ", jname));
  ac_ncall_ok_rfalse (env, get_table_prop_strict, (env, ntbl, "overdrive", &noverdrive, "%s (): ", jname));
  ac_ncall_ok_rfalse (env, get_table_prop_strict, (env, ntbl, "activePullup", &nactive_pullup, "%s (): ", jname));
  ac_ncall_ok_rfalse (env, get_table_prop_strict, (env, ntbl, "networkType", &nnetwork_type, "%s (): ", jname));
  overdrive_type_t overdrive;
  ac_ncall_ok_rfalse (env, get_overdrive, (env, noverdrive, &overdrive));
  active_pullup_type_t apu;
  ac_ncall_ok_rfalse (env, get_active_pullup, (env, nactive_pullup, &apu));
  network_type_t nettype;
  ac_ncall_ok_rfalse (env, get_network_type, (env, nnetwork_type, &nettype));
  ow_cfg->overdrive = overdrive;
  ow_cfg->active_pullup_type = apu;
  ow_cfg->network_type = nettype;
  return true;
}

static napi_value drupn_cleanup (napi_env env, napi_callback_info info) {
  struct finfo *finfo;
  ac_ncall_ok_rnull (env, get_args_and_data, (env, info, 0, NULL, (void **) &finfo, "drupn_cleanup (): "));
  drup_t drup = finfo->drup;
  if (drup) drup_destroy (drup);
  cleaned_up = true;
  napi_value undef;
  ac_ncall_nstatus_rnull (env, napi_get_undefined, (env, &undef));
  return undef;
}

static napi_value drupn_generate_secret (napi_env env, napi_callback_info info) {
  uint8_t ctrl_number, auth_number;
  uint8_t device_id[8];

  if (!check_cleaned_up_throw (env)) ac_piep_rn;
  napi_value argv[3];
  struct finfo *finfo;
  ac_ncall_ok_rnull (env, get_args_and_data, (env, info, 3, argv, (void **) &finfo, "drupn_generate_secret (): "));
  const char *jname = finfo->jname;
  drup_t_const drup = finfo->drup;
  napi_value nctrl_number = argv[0];
  napi_value nauth_number = argv[1];
  napi_value ndevice_id = argv[2];
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nctrl_number, &ctrl_number, "%s (): ", jname));
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nauth_number, &auth_number, "%s (): ", jname));
  if (!get_device_id (env, jname, ndevice_id, device_id)) ac_piep_rn;
  uint8_t secret_length = drup_get_secret_length_for_auth_num (drup, auth_number);
  if (secret_length == 0) ac_piep_rn;
  uint8_t secret[secret_length];

  bool rc = drup_generate_secret (drup, ctrl_number, auth_number, device_id, secret);
  if (!rc) throw_rn (env, "Error with %s () (lost contact or I/O error?)", jname);

  ac_debug ("drupn_generate_secret (): generated secret of length %d", secret_length);

  napi_value nsecret;
  napi_value nsecret_buffer;
  // --- no need to allocate.
  uint8_t *secret_buffer;

  ac_ncall_nstatus_rnull (env, napi_create_arraybuffer, (
    env, secret_length, (void**) &secret_buffer, &nsecret_buffer
  ));
  memcpy (secret_buffer, secret, secret_length);

  ac_ncall_nstatus_rnull (env, napi_create_typedarray, (
    env, napi_uint8_array, secret_length, nsecret_buffer, 0, &nsecret
  ));
  return nsecret;
}

static napi_value drupn_authenticate (napi_env env, napi_callback_info info) {
  uint8_t ctrl_number, auth_number;
  uint8_t device_id[8];
  bool authenticated;

  if (!check_cleaned_up_throw (env)) ac_piep_rn;
  napi_value argv[4];
  struct finfo *finfo;
  ac_ncall_ok_rnull (env, get_args_and_data, (env, info, 4, argv, (void **) &finfo, "drupn_authenticate (): "));
  const char *jname = finfo->jname;
  drup_t_const drup = finfo->drup;
  napi_value nctrl_number = argv[0];
  napi_value nauth_number = argv[1];
  napi_value ndevice_id = argv[2];
  napi_value nsecret = argv[3];
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nctrl_number, &ctrl_number, "%s (): ", jname));
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nauth_number, &auth_number, "%s (): ", jname));
  if (!get_device_id (env, jname, ndevice_id, device_id)) ac_piep_rn;

  struct drup_authenticator_view view;
  if (!drup_authenticator_view_at (drup, auth_number, &view)) ac_piep_rn;
  uint8_t secret_length = view.secret_length;
  const char *device_name = view.name;
  uint8_t secret[secret_length];

  {
    napi_typedarray_type type;
    size_t length;
    void *datav;
    size_t offset;
    ac_ncall_nstatus_rnull (env, napi_get_typedarray_info, (env, nsecret, &type, &length, &datav, NULL, &offset));
    if (type != napi_uint8_array) throw_rn (env, "%s (): `secret` must be a Uint8Array", jname);
    uint8_t *data = datav - offset;
    // --- note, we must make a copy because the underlying data arry can
    // get garbage collected.
    if (length == 16 && secret_length == 8) {
      ac_debug ("got legacy secret (16 bytes for 8), converting");
      legacy_ds1961_secret_16_to_8 (secret, data);
    }
    else if (length != secret_length) {
      throw_rn (env, "%s (): `secret` for %s must be a Uint8Array of length %d", jname, device_name, secret_length);
    }
    else memcpy (secret, data, secret_length);
  }

  ac_debug ("drupn_authenticate (): got secret of length %d", secret_length);
  bool rc = drup_authenticate (drup, ctrl_number, auth_number, device_id, secret, &authenticated);
  if (!rc) throw_rn (env, "Error with %s () (lost contact or I/O error?)", jname);

  napi_value nauthenticated;
  ac_ncall_nstatus_rnull (env, napi_get_boolean, (env, authenticated, &nauthenticated));
  return nauthenticated;
}

static napi_value drupn_search (napi_env env, napi_callback_info info) {
  uint8_t ctrl_number, auth_number;
  // --- @todo currently the library only returns max 1
  uint8_t max_results = 1;
  uint8_t device_ids[max_results][8];
  unsigned char device_id_strs[max_results][17];

  if (!check_cleaned_up_throw (env)) ac_piep_rn;
  napi_value argv[2];
  struct finfo *finfo;
  ac_ncall_ok_rnull (env, get_args_and_data, (env, info, 2, argv, (void **) &finfo, "drupn_search (): "));
  const char *jname = finfo->jname;
  drup_t_const drup = finfo->drup;
  napi_value nctrl_number = argv[0];
  napi_value nauth_number = argv[1];
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nctrl_number, &ctrl_number, "%s (): ", jname));
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nauth_number, &auth_number, "%s (): ", jname));
  uint8_t num_results;
  bool rc = drup_search (drup, ctrl_number, auth_number, max_results, device_ids, device_id_strs, &num_results);
  if (!rc) throw_rn (env, "%s () failed (I/O error?)", jname);

  napi_value nret_ary;
  ac_ncall_nstatus_rnull (env, napi_create_array_with_length, (env, num_results, &nret_ary));
  for (uint8_t i = 0; i < num_results; i++) {
    napi_value ndevice_id;
    ac_ncall_nstatus_rnull (env, napi_create_string_latin1, (env, (const char *) device_id_strs[i], 16, &ndevice_id));
    ac_ncall_nstatus_rnull (env, napi_set_element, (env, nret_ary, i, ndevice_id));
  }
  return nret_ary;
}

static napi_value drupn_get_num_buses (napi_env env, napi_callback_info info) {
  uint8_t ctrl_number;
  if (!check_cleaned_up_throw (env)) ac_piep_rn;
  napi_value argv[1];
  struct finfo *finfo;
  ac_ncall_ok_rnull (env, get_args_and_data, (env, info, 1, argv, (void **) &finfo, "drupn_get_num_buses (): "));
  const char *jname = finfo->jname;
  drup_t_const drup = finfo->drup;
  napi_value nctrl_number = argv[0];
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nctrl_number, &ctrl_number, "%s (): ", jname));
  uint8_t num_buses;
  ac_ncall_ok_rnull_msg (
    env, drup_get_num_buses, (drup, ctrl_number, &num_buses),
    "Unable to get number of buses (invalid controller number %d?) ", ctrl_number
  );
  napi_value ret;
  ac_ncall_nstatus_rnull (env, napi_create_double, (env, (double) num_buses, &ret));
  return ret;
}

static napi_value drupn_search_init (napi_env env, napi_callback_info info) {
  uint8_t ctrl_number, bus_number, auth_number;
  // --- stack is fine
  struct drup_bus_user_config ow_cfg;

  if (!check_cleaned_up_throw (env)) ac_piep_rn;
  napi_value argv[4];
  struct finfo *finfo;
  ac_ncall_ok_rnull (env, get_args_and_data, (env, info, 4, argv, (void **) &finfo, "drupn_search_init (): "));
  const char *jname = finfo->jname;
  drup_t_const drup = finfo->drup;
  napi_value nctrl_number = argv[0];
  napi_value nbus_number = argv[1];
  napi_value nauth_number = argv[2];
  napi_value now_cfg = argv[3];
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nctrl_number, &ctrl_number, "%s (): ", jname));
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nbus_number, &bus_number, "%s (): ", jname));
  ac_ncall_ok_rnull (env, get_value_uint8, (env, nauth_number, &auth_number, "%s (): ", jname));
  ac_ncall_ok_rnull (env, get_ow_cfg, (env, now_cfg, jname, &ow_cfg));

  presence_t presence;
  bool rc = drup_search_init (drup, ctrl_number, bus_number, auth_number, &ow_cfg, &presence);
  if (!rc) throw_rn (env, "%s () failed (I/O error?)", jname);
  napi_value nret;
  if (presence == PRESENCE_NONE) {
    ac_ncall_nstatus_rnull (env, napi_get_null, (env, &nret));
    return nret;
  }
  const char *ret =
    presence == PRESENCE_WITH_SHORT ? "presence-with-short" :
    presence == PRESENCE_READY ? "presence-ready" : NULL;
  if (ret == NULL) ac_piep_rn;
  ac_ncall_nstatus_rnull (env, napi_create_string_latin1, (env, ret, NAPI_AUTO_LENGTH, &nret));
  return nret;
}

static bool mkf (napi_env env, const char *jname, drup_t drup, napi_callback f, napi_value parent) {
  ac_try_malloc_rf (struct finfo *, finfo)
  *finfo = (struct finfo) {
    .jname = jname,
    .drup = drup,
  };
  napi_value nf;
  ac_ncall_nstatus_rfalse (env, napi_create_function, (env, jname, NAPI_AUTO_LENGTH, f, finfo, &nf));
  ac_ncall_nstatus_rfalse (env, napi_set_named_property, (env, parent, jname, nf));
  return true;
}

static napi_value drupn_init (napi_env env, napi_callback_info info) {
  const char *jname = get_jname (env, info);
  if (jname == NULL) ac_piep_rn;

  drup_t drup = init (env, info);
  if (drup == NULL) throw_rn (env, "%s (): failed", jname);

  napi_value nret;
  ac_ncall_nstatus_rnull (env, napi_create_object, (env, &nret));
  ac_ncall_ok_rnull (env, mkf, (env, "getNumBuses", drup, drupn_get_num_buses, nret));
  ac_ncall_ok_rnull (env, mkf, (env, "searchInit", drup, drupn_search_init, nret));
  ac_ncall_ok_rnull (env, mkf, (env, "search", drup, drupn_search, nret));
  ac_ncall_ok_rnull (env, mkf, (env, "authenticate", drup, drupn_authenticate, nret));
  ac_ncall_ok_rnull (env, mkf, (env, "generateSecret", drup, drupn_generate_secret, nret));
  ac_ncall_ok_rnull (env, mkf, (env, "cleanup", drup, drupn_cleanup, nret));
  return nret;
}

napi_value create_drupn_addon (napi_env env) {
  napi_value w1druppel;
  ac_ncall_nstatus_rnull (env, napi_create_object, (env, &w1druppel));
  ac_ncall_ok_rnull (env, mkf, (env, "init", NULL, drupn_init, w1druppel));
  return w1druppel;
}
