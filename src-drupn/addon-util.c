#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include <node_api.h>

#include <alleycat.h>

#include "addon-util.h"

#define mk_onerror(buf, n, fmt, vargs) do { \
  if (onerror_fmt == NULL) onerror[0] = '\0'; \
  else vsnprintf (onerror, n, onerror_fmt, vargs); \
} while (0)

bool is_undefined (napi_env env, napi_value nval, bool *ret) {
  napi_valuetype result;
  ac_ncall_nstatus_rfalse (env, napi_typeof, (env, nval, &result));
  *ret = result == napi_undefined;
  return true;
}

bool get_table_prop_strict (napi_env env, napi_value tbl, const char *key, napi_value *ret, const char *onerror_fmt, ...) {
  ac_ncall_nstatus_rfalse (env, napi_get_named_property, (env, tbl, key, ret));
  bool undefined;
  ac_ncall_ok_rfalse (env, is_undefined, (env, *ret, &undefined));
  if (!undefined) return true;

  va_list args;
  va_start (args, onerror_fmt);
  char onerror[200];
  mk_onerror (onerror, 200, onerror_fmt, args);
  throw_rf (env, "%sexpected key “%s” in table", onerror, key);
  return true;
}

bool get_table_prop (napi_env env, napi_value tbl, const char *key, napi_value *ret, bool *undefined) {
  ac_ncall_nstatus_rfalse (env, napi_get_named_property, (env, tbl, key, ret));
  ac_ncall_ok_rfalse (env, is_undefined, (env, *ret, undefined));
  return true;
}

static bool size_t_array_contains (size_t n, const size_t *xs, size_t x) {
  for (size_t i = 0; i < n; i++)
    if (xs [i] == x) return true;
  return false;
}

/* `argv` can be NULL.
 * `argc` is an input-output parameter.
 */
static bool _get_args_and_data (napi_env env, napi_callback_info info, size_t good_sizes_n, size_t *good_sizes, size_t *argc, napi_value *argv, void **data, const char *onerror_fmt, va_list args) {
  // --- alters *argc
  ac_ncall_nstatus_rfalse (env, napi_get_cb_info, (env, info, argc, argv, NULL, data));
  if (size_t_array_contains (good_sizes_n, good_sizes, *argc)) return true;

  char onerror[200];
  mk_onerror (onerror, 200, onerror_fmt, args);
  throw_rf (env, "%s%s", onerror, "unexpected, got %zu arguments");
}

bool get_args (napi_env env, napi_callback_info info, size_t argc, napi_value *argv, const char *onerror_fmt, ...) {
  size_t m = argc;
  size_t good_sizes_n = 1;
  size_t good_sizes[] = { argc, };
  va_list args;
  va_start (args, onerror_fmt);
  return _get_args_and_data (env, info, good_sizes_n, good_sizes, &m, argv, NULL, onerror_fmt, args);
}

bool get_args_and_data (napi_env env, napi_callback_info info, size_t argc, napi_value *argv, void **data, const char *onerror_fmt, ...) {
  size_t m = argc;
  size_t good_sizes_n = 1;
  size_t good_sizes[] = { argc, };
  va_list args;
  va_start (args, onerror_fmt);
  return _get_args_and_data (env, info, good_sizes_n, good_sizes, &m, argv, data, onerror_fmt, args);
}

bool get_args_var (napi_env env, napi_callback_info info, size_t good_sizes_n, size_t *good_sizes, size_t *argc, napi_value *argv, const char *onerror_fmt, ...) {
  va_list args;
  va_start (args, onerror_fmt);
  return _get_args_and_data (env, info, good_sizes_n, good_sizes, argc, argv, NULL, onerror_fmt, args);
}

bool get_data (napi_env env, napi_callback_info info, void **data) {
  ac_ncall_nstatus_rfalse (env, napi_get_cb_info, (env, info, NULL, NULL, NULL, data));
  return true;
}

bool get_value_string_latin1 (napi_env env, napi_value nval, size_t n, char *ret) {
  // --- note: avoid passing NULL as last argument (it is allowed (see node source) but not documented).
  // --- safe even if buffer is too small.
  size_t s;
  ac_ncall_nstatus_rfalse (env, napi_get_value_string_latin1, (env, nval, ret, n, &s));
  return true;
}

bool get_value_string_latin1_check_truncated (napi_env env, napi_value nval, size_t n, char *ret, bool *truncated) {
  size_t l;
  // --- 3rd arg is NULL => get length (excl. null)
  ac_ncall_nstatus_rfalse (env, napi_get_value_string_latin1, (env, nval, NULL, 0, &l));
  if (!get_value_string_latin1 (env, nval, n, ret)) return false;
  *truncated = l + 1 > n;
  return true;
}

// --- does not implicitly convert the double (unlike napi_get_value_(u)intxx),
// and throws if the input is not an integer between 0 and 255.
bool get_value_uint8 (napi_env env, napi_value nval, uint8_t *ret, const char *onerror_fmt, ...) {
  double d;
  ac_ncall_nstatus_rfalse (env, napi_get_value_double, (env, nval, &d));
  uint8_t _ret = (uint8_t) d;
  if ((double) _ret != d) {
    va_list args;
    va_start (args, onerror_fmt);
    char onerror[200];
    mk_onerror (onerror, 200, onerror_fmt, args);
    throw_rf (env, "%s%f", onerror, d);
  }
  *ret = _ret;
  return true;
}

static napi_status _table_get_entries (napi_env env, napi_value jtbl, uint32_t *len, napi_value *ret) {
  napi_value res;
  napi_value *ptr = ret == NULL ? &res : ret;
  ac_ncall_nstatus_rstatus (env, napi_get_property_names, (env, jtbl, ptr));
  ac_ncall_nstatus_rstatus (env, napi_get_array_length, (env, *ptr, len));
  return napi_ok;
}

napi_status table_get_num_entries (napi_env env, napi_value jtbl, uint32_t *len) {
  return _table_get_entries (env, jtbl, len, NULL);
}

napi_status table_get_entries (napi_env env, napi_value jtbl, uint32_t *len, napi_value *ret) {
  return _table_get_entries (env, jtbl, len, ret);
}

bool table_expect_num_entries_throw (napi_env env, napi_value jtbl, uint32_t n, const char *onerror_fmt, ...) {
  uint32_t l;
  ac_ncall_nstatus_rfalse (env, table_get_num_entries, (env, jtbl, &l));
  char entry[8];
  snprintf (entry, 8, n == 1 ? "entry" : "entries");
  if (l != n) {
    va_list args;
    va_start (args, onerror_fmt);
    char onerror[200];
    mk_onerror (onerror, 200, onerror_fmt, args);
    throw_rf (env, "%sexpected table to have exactly %u %s (got %u)", onerror, n, entry, l);
  }
  return true;
}

bool expect_array_get_length (napi_env env, napi_value nval, uint32_t *ret, const char *onerror_fmt, ...) {
  bool is_array;
  ac_ncall_nstatus_rfalse (env, napi_is_array, (env, nval, &is_array));
  if (!is_array) {
    va_list args;
    va_start (args, onerror_fmt);
    char onerror[200];
    mk_onerror (onerror, 200, onerror_fmt, args);
    throw_rf (env, "%sexpected array", onerror);
  }
  ac_ncall_nstatus_rfalse (env, napi_get_array_length, (env, nval, ret));
  return true;
}

bool get_type (napi_env env, napi_value val, char **ret) {
  napi_valuetype t;
  ac_ncall_nstatus_rfalse (env, napi_typeof, (env, val, &t));
  if      (t == napi_object)    *ret = "object";
  else if (t == napi_undefined) *ret = "undefined";
  else if (t == napi_null)      *ret = "null";
  else if (t == napi_boolean)   *ret = "boolean";
  else if (t == napi_number)    *ret = "number";
  else if (t == napi_string)    *ret = "string";
  else if (t == napi_symbol)    *ret = "symbol";
  else if (t == napi_function)  *ret = "function";
  else if (t == napi_external)  *ret = "external";
  else if (t == napi_bigint)    *ret = "bigint";
  else ac_piep_rf;
  return true;
}

bool expect_type (napi_env env, napi_value val, const char *typename, const char *onerror_fmt, ...) {
  char *s = NULL;
  ac_ncall_ok_rfalse (env, get_type, (env, val, &s));
  if (strcmp (s, typename)) {
    va_list args;
    va_start (args, onerror_fmt);
    char onerror[200];
    mk_onerror (onerror, 200, onerror_fmt, args);
    throw_rf (env, "%sexpected %s (got %s)", onerror, typename, s);
  }
  return true;
}

napi_status string_length_latin1 (napi_env env, napi_value nval, size_t *ret) {
  return napi_get_value_string_latin1 (env, nval, NULL, 0, ret);
}
