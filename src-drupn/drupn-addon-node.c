#include <node_api.h>
#include "drupn-addon.h"

// --- must return napi_value
NAPI_MODULE_INIT() {
  // --- in scope: `napi_env env` and `napi_value exports`
  (void) exports;
  return create_drupn_addon (env);
}
