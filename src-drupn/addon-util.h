#ifndef _ADDON_UTIL_H
#define _ADDON_UTIL_H

#include <stdbool.h>

#include <node_api.h>
#include <js_native_api.h>

#include "drup.h"

#define _ac_ncall_onerror(env, onerror_ret, fmt, args...) do { \
  const napi_extended_error_info *error_info = NULL; \
  napi_get_last_error_info ((env), &error_info); \
  bool is_pending; \
  napi_is_exception_pending ((env), &is_pending); \
  if (!is_pending) { \
    const char *msg1$ = (error_info->error_message == NULL) \
        ? "" \
        : error_info->error_message; \
    char msg2$[200]; \
    snprintf (msg2$, 200, fmt "%s", ## args, msg1$); \
    napi_throw_error ((env), NULL, msg2$); \
  } \
  return onerror_ret; \
} while (0)

#define _ac_ncall_nstatus(onerror_ret, env, f, args, fmt...) do { \
  /* Note, don't rename (see ac_ncall_nstatus_rstatus) */ \
  napi_status status$ = (f args); \
  if (status$ != napi_ok) \
    _ac_ncall_onerror (env, onerror_ret, fmt); \
} while (0)

// @todo bools and napi_status clash and C's type system is no help if
// someone accidentally uses this for call which returns napi_status.
// Luckily, napi_status ok is exactly 1, so this is mostly not a problem.
#define _ac_ncall_ok(onerror_ret, env, f, args, fmt...) do { \
  if ((f args) != true) \
    _ac_ncall_onerror (env, onerror_ret, fmt); \
} while (0)

/* Use the ac_ncall_nstatus macros when the inner call returns napi_status.
 * An error during the inner call means something went wrong with napi. An
 * exception will be generated with the error messages in the error queue
 * and thrown, unless the inner call has an exception pending.
 */

// --- outer function will return a pointer; i.e., we will return NULL on
// error. Note that napi_value is a pointer.
#define ac_ncall_nstatus_rnull(env, f, args) _ac_ncall_nstatus (NULL, env, f, args, "")
// --- outer function will return napi_status; i.e., we will return the
// status of the failed call on error.
#define ac_ncall_nstatus_rstatus(env, f, args) _ac_ncall_nstatus (status$, env, f, args, "")
// --- outer function will return bool; i.e., we return false on error.
#define ac_ncall_nstatus_rfalse(env, f, args) _ac_ncall_nstatus (false, env, f, args, "")

#define ac_ncall_nstatus_rnull_msg(env, f, args, fmt...) _ac_ncall_nstatus (NULL, env, f, args, fmt)
#define ac_ncall_nstatus_rstatus_msg(env, f, args, fmt...) _ac_ncall_nstatus (status$, env, f, args, fmt)
#define ac_ncall_nstatus_rfalse_msg(env, f, args, fmt...) _ac_ncall_nstatus (false, env, f, args, fmt)

/* Use the ac_ncall_ok macros when the inner call returns bool. In many cases,
 * the inner call will throw an exception, so the callok_ macros in general
 * don't throw one. Use ac_ncall_ok_throw_rnull, and pass it an error message, if
 * you do want to throw one.
 */

// --- outer function will return a pointer; i.e., we will return NULL on
// error. Note that napi_value is a pointer.
#define ac_ncall_ok_rnull(env, f, args) _ac_ncall_ok (NULL, env, f, args, "")
// --- outer function will return bool; i.e., we return false on error.
#define ac_ncall_ok_rfalse(env, f, args) _ac_ncall_ok (false, env, f, args, "")

#define ac_ncall_ok_rnull_msg(env, f, args, fmt...) _ac_ncall_ok (NULL, env, f, args, fmt)
#define ac_ncall_ok_rfalse_msg(env, f, args, fmt...) _ac_ncall_ok (false, env, f, args, fmt)

#define _throw_r(ret, env, fmt...) do { \
  char err_str[200]; \
  snprintf (err_str, 200, fmt); \
  napi_throw_error (env, NULL, err_str); \
  return ret; \
} while (0)

#define throw_rf(env, fmt...) _throw_r (false, env, fmt)
#define throw_rn(env, fmt...) _throw_r (NULL, env, fmt)

struct finfo {
  const char *jname;
  drup_t drup;
};

// --- @future: get_table_props would be nice, to get several keys at once
// from a table.
bool get_table_prop_strict (napi_env env, napi_value tbl, const char *key, napi_value *ret, const char *onerror_fmt, ...);
bool get_table_prop (napi_env env, napi_value tbl, const char *key, napi_value *ret, bool *undefined);
bool get_args (napi_env env, napi_callback_info info, size_t argc, napi_value *argv, const char *onerror_fmt, ...);
bool get_args_var (napi_env env, napi_callback_info info, size_t good_sizes_n, size_t *good_sizes, size_t *argc, napi_value *argv, const char *onerror_fmt, ...);
bool get_args_and_data (napi_env env, napi_callback_info info, size_t argc, napi_value *argv, void **data, const char *onerror_fmt, ...);
bool get_data (napi_env env, napi_callback_info info, void **data);
bool get_value_string_latin1 (napi_env env, napi_value nval, size_t n, char *ret);
bool get_value_string_latin1_check_truncated (napi_env env, napi_value nval, size_t n, char *ret, bool *truncated);
napi_status string_length_latin1 (napi_env env, napi_value nval, size_t *ret);
bool get_value_uint8 (napi_env env, napi_value nval, uint8_t *ret, const char *onerror_fmt, ...);
napi_status table_get_num_entries (napi_env env, napi_value jtbl, uint32_t *len);
napi_status table_get_entries (napi_env env, napi_value jtbl, uint32_t *len, napi_value *ret);
bool get_type (napi_env env, napi_value val, char **ret);
bool expect_type (napi_env env, napi_value val, const char *typename, const char *onerror_fmt, ...);
bool is_undefined (napi_env env, napi_value nval, bool *ret);
bool table_expect_num_entries_throw (napi_env env, napi_value jtbl, uint32_t n, const char *onerror_fmt, ...);
bool expect_array_get_length (napi_env env, napi_value nval, uint32_t *ret, const char *onerror_fmt, ...);

#endif
