#include <stddef.h>

#include <i2c-dev-linux.h>

#include <alleycat.h>

#include "internal/controller.h"
#include "internal/i2c.h"
#include "internal/util.h"

bool i2c_read_byte (drup_controller_t_const ctrl, uint8_t * const ret) {
  int val = i2c_smbus_read_byte (ctrl->fd);
  if (val == -1) ac_warn_perr_rf (
    "i2c_read_byte (): failed to read byte"
  );
  if (ret != NULL) *ret = (uint8_t) val;
  return true;
}

bool i2c_cmd (drup_controller_t_const ctrl, uint8_t cmd) {
  if (i2c_smbus_write_byte (ctrl->fd, cmd) != 0) ac_warn_perr_rf (
    "i2c write cmd failed"
  );
  return true;
}

/* Send command and one byte of data.
 */
bool i2c_cmd_with_data (drup_controller_t_const ctrl, uint8_t cmd, uint8_t data) {
  if (i2c_smbus_write_byte_data (ctrl->fd, cmd, data) != 0) ac_warn_perr_rf (
    "i2c write cmd + 1 byte of data failed"
  );
  return true;
}

/* Send command and a block of data.
 */
bool i2c_cmd_with_block (drup_controller_t_const ctrl, uint8_t const cmd, uint8_t const length, const uint8_t * const data) {
  if (i2c_smbus_write_block_data (ctrl->fd, cmd, length, data) != 0) ac_warn_perr_rf (
    "i2c write cmd + block of data failed"
  );
  return true;
}
