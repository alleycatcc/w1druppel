#include <stdint.h>
#include <string.h>

#include <alleycat.h>

#include "drup.h"
#include "internal/authenticator.h"
#include "internal/bus.h"
#include "internal/controller.h"
#include "internal/drup.h"
#include "internal/i2c.h"
#include "internal/drup.h"
#include "internal/util.h"

static bool mk_apu (active_pullup_type_t apu, network_type_t net) {
  if (!validate_active_pullup_type (apu)) ac_piep_rf;
  if (!validate_network_type (net)) ac_piep_rf;
  return apu == APU_AUTO_NEW ? true :
    apu == APU_AUTO_OLD ? (net == MULTI_DROP) :
    apu == APU_FORCE_OFF ? false :
    // --- apu == APU_FORCE_ON
    true;
}

static bool mk_ow_config (drup_t_const drup, uint8_t auth_num, struct drup_bus_user_config *cfg, bool is_init, struct ow_config *ret) {
  struct drup_authenticator_view auth_view;
  try_with (drup_authenticator_view_at, (drup, auth_num, &auth_view), ac_err_rf);
  overdrive_type_t overdrive_requested = cfg->overdrive;
  if (!validate_overdrive_type (overdrive_requested)) ac_piep_rf;
  bool overdrive = overdrive_requested == OVERDRIVE_AUTO ? auth_view.overdrive_default :
    overdrive_requested == OVERDRIVE_FORCE_OFF ? false :
    // --- overdrive_requested == OVERDRIVE_MOSTLY_ON
    true;
  bool disable = is_init && auth_view.disable_overdrive_during_init;
  ret->overdrive = overdrive && !disable;
  ret->active_pullup = mk_apu (cfg->active_pullup_type, cfg->network_type);
  return true;
}

drup_t drup_mk (drup_controller_t *ctrls, uint8_t numc, drup_authenticator_t *auths, uint8_t numa, bool verbose, bool mock) {
  ac_try_malloc_rnull (struct drup_impl *, t)
  for (uint8_t ctrl_num = 0; ctrl_num < numc; ctrl_num++) {
    drup_controller_t ctrl = ctrls[ctrl_num];
    if (!controller_init (ctrl, mock))
      ac_piep_rn;
  }
  ac_try_malloc_rnull (struct config *, config)
  config->verbose = verbose;
  t->config = config;
  t->controllers = ctrls;
  t->num_controllers = numc;
  t->authenticators = auths;
  t->num_authenticators = numa;
  return t;
}

void drup_destroy (drup_t drup) {
  if (!drup) return;
  for (uint8_t i = 0; i < drup->num_controllers; i++)
    drup_controller_destroy (drup->controllers[i]);
  for (uint8_t i = 0; i < drup->num_authenticators; i++)
    drup_authenticator_destroy (drup->authenticators[i]);
  free (drup->config);
  free (drup);
}

static bool validate_ctrl_number (drup_t_const drup, uint8_t ctrl_num) {
  return ctrl_num <= drup->num_controllers - 1;
}

static bool validate_bus_number (drup_t_const drup, uint8_t ctrl_num, uint8_t bus_num) {
  drup_controller_t ctrl = drup->controllers[ctrl_num];
  int num_buses = ctrl->spec->num_buses;
  if (bus_num >= num_buses) ac_warn_rf (
    "%s (): invalid bus number %d", __func__, bus_num
  );
  const struct bus *bus = ctrl->buses[bus_num];
  if (bus == NULL) ac_warn_rf (
    "%s (): no bus found at number %d", __func__, bus_num
  );
  return true;
}

static bool validate_auth_number (drup_t_const drup, uint8_t auth_num) {
  return auth_num <= drup->num_authenticators - 1;
}

bool drup_search_init (
    drup_t_const drup,
    uint8_t ctrl_num, uint8_t bus_num, uint8_t auth_num,
    struct drup_bus_user_config *user_cfg,
    presence_t *presence
) {
  try_with (validate_ctrl_number, (drup, ctrl_num), ac_err_rf);
  try_with (validate_bus_number, (drup, ctrl_num, bus_num), ac_err_rf);
  try_with (validate_auth_number, (drup, auth_num), ac_err_rf);
  drup_controller_t ctrl = drup->controllers[ctrl_num];
  struct bus *bus = ctrl->buses[bus_num];
  bool ok = mk_ow_config (drup, auth_num, user_cfg, true, bus->ow_config);
  bus->network_type = user_cfg->network_type;
  if (!ok) ac_piep_rf;
  if (bus->ow_config == NULL) ac_piep_rf;
  try (controller_search_init, (ctrl, bus, bus->ow_config, presence));
  return true;
}

bool drup_search (
    drup_t_const drup, uint8_t ctrl_num, uint8_t auth_num, uint8_t max_results,
    uint8_t device_ids[][8], unsigned char device_id_strs[][17], uint8_t *num_results
) {
  try_with (validate_ctrl_number, (drup, ctrl_num), ac_err_rf);
  try_with (validate_auth_number, (drup, auth_num), ac_err_rf);
  memset (device_ids, 0, 8 * max_results);
  drup_controller_t ctrl = drup->controllers[ctrl_num];
  drup_authenticator_t_const auth = drup->authenticators[auth_num];
  try (controller_search, (ctrl, auth, max_results, device_ids, num_results));
  for (uint8_t i = 0; i < *num_results; i++)
    uint8_to_hex (device_id_strs[i], device_ids[i]);
  return true;
}

bool drup_authenticate (
    drup_t_const drup,
    uint8_t ctrl_num, uint8_t auth_num,
    uint8_t *device_id, const uint8_t *secret, bool *authenticated
) {
  try_with (validate_ctrl_number, (drup, ctrl_num), ac_err_rf);
  try_with (validate_auth_number, (drup, auth_num), ac_err_rf);
  drup_controller_t_const ctrl = drup->controllers[ctrl_num];
  drup_authenticator_t auth = drup->authenticators[auth_num];

  auth_transaction_t transaction = auth_transaction_mk (ctrl, device_id, secret);
  bool ok = authenticator_attempt (auth, transaction, authenticated);
  auth_transaction_destroy (transaction);
  return ok;
}

bool drup_generate_secret (
    drup_t_const drup,
    uint8_t ctrl_num, uint8_t auth_num,
    uint8_t *device_id, uint8_t *secret
) {
  try_with (validate_ctrl_number, (drup, ctrl_num), ac_err_rf);
  try_with (validate_auth_number, (drup, auth_num), ac_err_rf);
  drup_controller_t_const ctrl = drup->controllers[ctrl_num];
  drup_authenticator_t auth = drup->authenticators[auth_num];
  gensecret_transaction_t transaction = gensecret_transaction_mk (ctrl, device_id);
  bool ok = authenticator_generate_secret (auth, transaction, secret);
  // --- the datasheet for DS1964 says we need to do this if load and lock
  // secret fails, so we do it here (seems unnecessary since the next
  // command will begin with a reset anyway, but ok)
  if (!ok) try (controller_w1_reset_external, (ctrl));
  gensecret_transaction_destroy (transaction);
  return ok;
}

void drup_authenticator_views (drup_t_const drup, struct drup_authenticator_view *ret) {
  uint8_t num_authenticators = drup->num_authenticators;
  for (uint8_t i = 0; i < num_authenticators; i++)
    ret[i] = drup_authenticator_view (drup->authenticators[i]);
}

bool drup_authenticator_view_at (drup_t_const drup, uint8_t auth_num, struct drup_authenticator_view *view) {
  try_with (validate_auth_number, (drup, auth_num), ac_err_rf);
  *view = drup_authenticator_view (drup->authenticators[auth_num]);
  return true;
}

struct drup_view drup_view (drup_t_const drup) {
  struct drup_view t = {
    .num_controllers = drup->num_controllers,
    .num_authenticators = drup->num_authenticators,
  };
  return t;
}

bool drup_get_num_buses (drup_t_const drup, uint8_t ctrl_num, uint8_t *ret) {
  try_with (validate_ctrl_number, (drup, ctrl_num), ac_err_rf);
  *ret = drup_controller_num_buses (drup->controllers[ctrl_num]);
  return true;
}
