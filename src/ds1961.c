#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include <sha33/sha33.h>

#include <alleycat.h>

#include "internal/drup.h"
#include "internal/authenticator.h"
#include "internal/controller.h"
#include "internal/crc.h"
#include "internal/util.h"

#include "internal/ds1961.h"

#define AUTH_ADDR 0

#define EXTRA_WAIT_PROG false
#define NUM_RETRIES_WRITE_SECRET 1
#define NUM_RETRIES_WRITE_SCRATCHPAD 1
#define NUM_RETRIES_READ_AUTH_PAGE 1
#define NUM_RETRIES_AUTH 3

static bool inv_crc16_validate (const uint8_t * const data, uint8_t length) {
  uint8_t byte_count = length - 2;
  uint8_t byte1_crc_expected = length - 2;
  uint8_t byte2_crc_expected = length - 1;
  return crc_validate16 (
    data, byte_count,
    ~data[byte1_crc_expected],
    ~data[byte2_crc_expected]
  );
}

/* Can fail due to CRC error, which doesn't set errno.
 */
static bool read_auth_page (transaction_t transaction, uint16_t auth_addr, uint8_t * const rmac_read, uint8_t * const rmac) {
  try (transaction_prepare_device_command, (transaction, CMD_READ_AUTH_PAGE));

  drup_controller_t_const ctrl = transaction->ctrl;
  uint8_t write[2];
  write[0] = (auth_addr >> 0) & 0xFF;
  write[1] = (auth_addr >> 8) & 0xFF;
  try (controller_write_bytes, (ctrl, write, 2));

  uint8_t read1[35];
  try (controller_read_bytes, (ctrl, read1, 35));
  if (read1[32] != 0xFF) ac_warn_rf (
    "%s (): got 0x%02x (expected 0xFF)", __func__, read1[32]
  );

  uint8_t data_crc[38];
  data_crc[0] = CMD_READ_AUTH_PAGE;
  memcpy (data_crc + 1, write, 2);
  memcpy (data_crc + 3, read1, 35);
  try_with_info (inv_crc16_validate, (data_crc, 38), ac_warn_rf, "(crc 1)");

  memcpy (rmac_read, read1, 32);

  nanosleep (&T_CSHA, NULL);
  uint8_t read_mac[20];
  try (controller_read_bytes, (ctrl, read_mac, 20));
  memcpy (rmac, read_mac, 20);

  uint8_t data_crc2[22];
  memcpy (data_crc2, read_mac, 20);
  try (controller_read_bytes, (ctrl, data_crc2 + 20, 2));
  try_with_info (inv_crc16_validate, (data_crc2, 22), ac_warn_rf, "(crc 2)");

  uint8_t status;
  try (controller_read_byte, (ctrl, &status));
  if (status != 0xAA) {
    ac_warn ("%s: failed due to status = %x", __func__, status);
    return false;
  }
  return true;
}

static bool write_scratchpad (transaction_t transaction, uint16_t addr, const uint8_t * const data) {
  drup_controller_t_const ctrl = transaction->ctrl;
  try (transaction_prepare_device_command, (transaction, CMD_WRITE_SCRATCHPAD));

  uint8_t write[10];
  write[0] = (addr >> 0) & 0xFF;
  write[1] = (addr >> 8) & 0xFF;
  memcpy (write + 2, data, 8);
  try (controller_write_bytes, (ctrl, write, 10));

  uint8_t data_crc[13];
  data_crc[0] = CMD_WRITE_SCRATCHPAD;
  memcpy (data_crc + 1, write, 10);
  try (controller_read_bytes, (ctrl, data_crc + 11, 2));
  try_with (inv_crc16_validate, (data_crc, 13), ac_warn_rf);
  return true;
}

/* Doesn't always set errno.
 */
static bool read_with_challenge (transaction_t transaction, const uint8_t * const challenge, uint8_t * const auth_page_read, uint8_t * const rmac) {
  // --- 10ms
  // static const struct timespec pass_wait = { .tv_sec = 0, .tv_nsec = 10e6, };
  // --- 100ms
  // static const struct timespec pass_wait2 = { .tv_sec = 0, .tv_nsec = 1e8, };

  uint8_t data[8] = {
    0, 0, 0, 0, challenge[0], challenge[1], challenge[2], 0,
  };
  /*
  bool ok;

  ok = false;
  for (int loop = 0; loop < NUM_RETRIES_WRITE_SCRATCHPAD; loop++) {
    if (write_scratchpad (transaction, AUTH_ADDR, data)) {
      ok = true;
      break;
    }
    ac_warn ("%s () failed (pass %d/%d)", __func__, loop + 1, NUM_RETRIES_WRITE_SCRATCHPAD);
    nanosleep (&pass_wait2, NULL);
  }
  if (!ok) return false;

  ok = false;
  for (int loop = 0; loop < NUM_RETRIES_READ_AUTH_PAGE; loop++) {
    if (read_auth_page (transaction, AUTH_ADDR, auth_page_read, rmac)) {
      ok = true;
      break;
    }
    ac_warn ("%s () failed (pass %d/%d)", __func__, loop + 1, NUM_RETRIES_READ_AUTH_PAGE);
    nanosleep (&pass_wait, NULL);
  }
  if (!ok) return false;
  */

  try (write_scratchpad, (transaction, AUTH_ADDR, data));
  try (read_auth_page, (transaction, AUTH_ADDR, auth_page_read, rmac));

  return true;
}

static void calc_lmac (uint16_t auth_addr, const uint8_t *device_id, const uint8_t * const challenge, const uint8_t * const secret8, const uint8_t * const auth_page_read, uint8_t * const lmac) {
  uint32_t hash_input[16];
  uint32_t hash_output[16];
  uint8_t id_input[7];

  for (int i = 0; i < 7; i++)
    id_input[i] = device_id[i];

  hash_input[0] = (secret8[0] << 24) | (secret8[1] << 16) | (secret8[2] << 8) | secret8[3];

  for (int i = 0; i < 32; i += 4)
    hash_input[i/4 + 1] = (auth_page_read[i] << 24) | (auth_page_read[i + 1] << 16) | (auth_page_read[i + 2] << 8) | auth_page_read[i + 3];

  hash_input[9] = 0xFFFFFFFF;
  uint8_t mp = (0b1000 << 3) | ((auth_addr >> 5) & 0b111);
  hash_input[10] = (mp << 24) | (id_input[0] << 16) | (id_input[1] << 8) | id_input[2];
  hash_input[11] = (id_input[3] << 24) | (id_input[4] << 16) | (id_input[5] << 8) | id_input[6];
  hash_input[12] = (secret8[4] << 24) | (secret8[5] << 16) | (secret8[6] << 8) | secret8[7];
  hash_input[13] = (challenge[0] << 24) | (challenge[1] << 16) | (challenge[2] << 8) | 0x80;
  hash_input[14] = 0;
  hash_input[15] = 0x1B8;

  sha33_compute_sha_vm_32 (hash_input, hash_output);
  sha33_hash_to_mac (hash_output, lmac);
}

static bool read_scratchpad (transaction_t transaction, uint16_t *addr, uint8_t *es) {
  drup_controller_t_const ctrl = transaction->ctrl;
  try (transaction_prepare_device_command, (transaction, CMD_READ_SCRATCHPAD));

  uint8_t data_read[3];
  try (controller_read_bytes, (ctrl, data_read, 3));
  *addr = (data_read[1] << 8) | data_read[0];
  *es = data_read[2];

  uint8_t read[8];
  try (controller_read_bytes, (ctrl, read, 8));

  uint8_t data_crc[14];
  data_crc[0] = CMD_READ_SCRATCHPAD;
  memcpy (data_crc + 1, data_read, 3);
  memcpy (data_crc + 4, read, 8);
  try (controller_read_bytes, (ctrl, data_crc + 12, 2));

  try_with (inv_crc16_validate, (data_crc, 14), ac_warn_rf);
  return true;
}

static bool load_first_secret (transaction_t transaction, uint16_t addr, uint8_t es) {
  drup_controller_t_const ctrl = transaction->ctrl;
  uint8_t write[3] = {
    (addr >> 0) & 0xFF,
    (addr >> 8) & 0xFF,
    es,
  };

  try (transaction_prepare_device_command, (transaction, CMD_LOAD_FIRST_SECRET));
  try (controller_write_bytes, (ctrl, write, 3));

  nanosleep (&T_PROG, NULL);
  if (EXTRA_WAIT_PROG) nanosleep (&T_PROG, NULL);
  uint8_t status;
  try (controller_read_byte, (ctrl, &status));
  return status == 0xAA;
}

static bool write_secret (transaction_t transaction, const uint8_t *secret) {
  try (write_scratchpad, (transaction, MEM_REGION_SECRET, secret));

  uint16_t addr;
  uint8_t es;
  try (read_scratchpad, (transaction, &addr, &es));
  try_with (load_first_secret, (transaction, addr, es), ac_warn_rf);
  return true;
}

bool ds1961_generate_secret (gensecret_transaction_t transaction, uint8_t * const secret) {
  // --- 10ms
  static const struct timespec pass_wait = { .tv_sec = 0, .tv_nsec = 10e6, };

  if (!rand_bytes (secret, 8)) ac_piep_rf;
  for (int loop = 0; loop < NUM_RETRIES_WRITE_SECRET; loop++) {
    if (write_secret (&transaction->base, secret))
      return true;
    ac_warn ("write_secret (): failed (pass %d/%d)", loop + 1, NUM_RETRIES_WRITE_SECRET);
    nanosleep (&pass_wait, NULL);
  }
  ac_warn ("ds1961_generate_secret (): failed at write_secret ()");
  return false;
}

bool ds1961_auth_attempt (auth_transaction_t transaction, bool *authenticated) {
  const uint8_t *device_id = transaction->base.device_id;

  uint8_t challenge[3];
  if (!rand_bytes (challenge, 3)) ac_piep_rf;

  // --- read during calculation of rmac.
  uint8_t auth_page_read[32];
  // --- calculated by device.
  uint8_t rmac[20];
  // --- calculated by us.
  uint8_t lmac[20];

  // --- 5ms
  static const struct timespec pass_wait = { .tv_sec = 0, .tv_nsec = 5e6, };
  for (uint8_t i = 0; i < NUM_RETRIES_AUTH; i++) {
    if (read_with_challenge (&transaction->base, challenge, auth_page_read, rmac))
      break;
    ac_warn ("read_with_challenge failed, pass %d/%d", i + 1, NUM_RETRIES_AUTH);
    nanosleep (&pass_wait, NULL);
  }

  const uint8_t *secret = transaction->secret;
  calc_lmac (AUTH_ADDR, device_id, challenge, secret, auth_page_read, lmac);
  *authenticated = memcmp (lmac, rmac, sizeof rmac) == 0;
  return true;
}
