#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <openssl/err.h>
#include <openssl/rand.h>

#include <alleycat.h>

#include "internal/util.h"

/* `ret`: uint8_t[17]
 */
void uint64_to_hex (uint64_t const value, unsigned char *ret) {
  for (uint8_t i = 0; i < 8; i++) {
    uint8_t byte = value >> i*8;
    sprintf ((char *) ret + i*2, "%02X", byte);
  }
}

void uint8_to_hex (unsigned char *ret, uint8_t *value) {
  for (uint8_t i = 0; i < 8; i++)
    sprintf ((char *) ret + i*2, "%02X", value[i]);
}

bool rand_bytes (uint8_t *b, int n) {
  if (RAND_bytes (b, n) != 1) {
    uint64_t e;
    char buf[256];
    do {
      e = ERR_get_error ();
      ERR_error_string (e, buf);
      ac_err ("%s (): failed to get random byte(s): openssl error: %s", __func__, buf);
    } while (e != 0);
    return false;
  }
  return true;
}

bool rand_byte (uint8_t *b) {
  return rand_bytes (b, 1);
}

bool rand_bool (bool *b) {
  uint8_t c;
  bool ok = rand_byte (&c);
  if (!ok) return false;
  *b = (c * 1.0 / 0xff) > 0.5;
  return true;
}

int max_int (uint8_t num_args, ...) {
  int res = INT_MIN;
  va_list ap;
  va_start (ap, num_args);
  for (uint8_t i = 0; i < num_args; i++) {
    int x = va_arg (ap, int);
    if (i == 0 || x > res) res = x;
  }
  va_end (ap);
  return res;
}

/* `ms`: (0, 10000]
 */
bool sleep_ms (unsigned int ms) {
  if (ms == 0 || ms > 10000) ac_piep_rf;
  int s = (int) lround (ms / 1000.0);
  int rest = ms - s*1000;
  struct timespec t = { .tv_sec = s, .tv_nsec = rest * 1e6, };
  struct timespec rem;
  errno = 0;
  if (nanosleep (&t, &rem)) {
    if (errno == EINTR) return sleep_ms ((int) (rem.tv_sec * 1000.0 + rem.tv_nsec / 1000.0));
    return false;
  }
  return true;
}
