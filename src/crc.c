#include <stdbool.h>
#include <stdint.h>

#include "internal/crc.h"

static uint16_t build16 (const uint8_t * const data, uint8_t const  num_bytes) {
  const uint8_t * data_ptr = data;
  uint16_t crc = 0x0000;
  for (int i = 0; i < num_bytes; i++)
    crc = CRC16[(crc ^ *data_ptr++) & 0xff] ^ (crc >> 8);
  return crc;
}

bool crc_validate16 (const uint8_t * const data, uint8_t const num_bytes, uint8_t const crc16_byte1, uint8_t const crc16_byte2) {
  uint16_t const crc16 = build16 (data, num_bytes);
  return (uint8_t) crc16 == crc16_byte1 && (uint8_t) (crc16 >> 8) == crc16_byte2;
}
