#include <errno.h>
#include <stdint.h>
#include <string.h>

#include <alleycat.h>

/* `value`: unsigned char[17], `ret`: uint8_t[8]
 */
void drup_hex_to_uint8 (const unsigned char * const value, uint8_t *ret) {
  for (int i = 0; i < 8; i++) {
    int j = i * 2;
    char hex[3];
    strncpy (hex, (char *) value + j, 2);
    hex[2] = '\0';
    errno = 0;
    uint8_t byte = strtol (hex, NULL, 16);
    if (errno == EINVAL || errno == ERANGE) ac_warn ("hex_to_uint8 (): error converting hex val %s", hex);
    ret[i] = byte;
  }
}

/* `value`: unsigned char[17]
 */
uint64_t drup_hex_to_uint64 (const unsigned char * const value) {
  uint64_t result = 0;
  uint8_t buf[8];
  drup_hex_to_uint8 (value, buf);
  for (int i = 0; i < 8; i++) {
    uint8_t byte = buf[i];
    result |= (uint64_t) byte << i*8;
  }
  return result;
}

/* `src16`: 16 * uint8_t
 * `tgt8`: 8 * uint8_t
 */
void legacy_ds1961_secret_16_to_8 (uint8_t *tgt8, const uint8_t *src16) {
  for (int i = 0; i < 8; i++)
    tgt8[i] = src16[2 * i];
}
