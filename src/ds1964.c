#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <string.h>
#include <time.h>

#include <alleycat.h>
#include <crc16.h>
#include <sha256.h>

#include "internal/drup.h"
#include "internal/authenticator.h"
#include "internal/ds1964.h"
#include "internal/util.h"
#include "internal/util-debug-buffer.h"

#define AUTH_PAGE_NUM 0x01

#define debug_mac(which, data) \
  debug_buf (data, uint8_t, 32, 8, "MAC (%s)", which);

#define GENSECRET_RETRY_MS 200
#define AUTH_RETRY_MS 100

// --- 10ms
static const struct timespec T_PRD = { .tv_sec = 0, .tv_nsec = 10e6, };
// --- 100ms
static const struct timespec T_PRS = { .tv_sec = 0, .tv_nsec = 100e6, };
// --- 3ms
static const struct timespec T_CSHA = { .tv_sec = 0, .tv_nsec = 3e6, };

// --- `data` must be 32 bytes long.
static bool write_scratchpad (transaction_t transaction, const uint8_t * const data, bool init_rom_function) {
  drup_controller_t_const ctrl = transaction->ctrl;
  uint8_t param_write = 0x00;
  uint8_t send[] = {
    COMMAND_SCRATCHPAD,
    param_write,
  };
  uint8_t crc16[2];
  if (init_rom_function)
    try (transaction_prepare_device_command, (transaction, 0));
  try_info (controller_write_bytes, (ctrl, send, 2), "write 2");
  try_info (controller_read_bytes, (ctrl, crc16, 2), "read 2");
  if (crc16[0] == 0xff && crc16[1] == 0xff)
    ac_warn_rf ("write_scratchpad (): bad param");
  if (!crc16_check (send, 2, crc16[0], crc16[1]))
    ac_warn_rf ("write_scratchpad (): failed CRC (1st pass)");
  try_info (controller_write_bytes, (ctrl, data, 32), "write 32");
  try_info (controller_read_bytes, (ctrl, crc16, 2), "write 2");
  if (!crc16_check (data, 32, crc16[0], crc16[1]))
    ac_warn_rf ("write_scratchpad (): failed CRC (2nd pass)");
  debug_buf (data, uint8_t, 32, 8, "scratchpad (write)");
  return true;
}

static bool read_scratchpad (transaction_t transaction, uint8_t *ret_data) {
  drup_controller_t_const ctrl = transaction->ctrl;
  uint8_t param_read = 0x0f;
  uint8_t send[] = {
    COMMAND_SCRATCHPAD,
    param_read,
  };
  uint8_t crc16[2];
  try (controller_write_bytes, (ctrl, send, 2));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (!crc16_check (send, 2, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): read scratchpad: failed CRC (1st pass)", __func__);
  uint8_t read[32];
  try (controller_read_bytes, (ctrl, read, 32));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (!crc16_check (read, 32, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): read scratchpad: failed CRC (2nd pass)", __func__);
  if (ret_data != NULL) memcpy (ret_data, read, 32);
  debug_buf (read, uint8_t, 32, 8, "scratchpad (read)");
  return true;
}

static bool load_and_lock (transaction_t transaction) {
  drup_controller_t_const ctrl = transaction->ctrl;
  const uint8_t param_no_lock = 0x00 | (1 << 4);
  uint8_t send[] = {
    COMMAND_LOAD_AND_LOCK,
    param_no_lock,
  };
  uint8_t crc16[2];
  uint8_t cs;
  try (controller_write_bytes, (ctrl, send, 2));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (crc16[0] == 0xff && crc16[1] == 0xff)
    ac_warn_rf ("%s (): load and lock secret: bad param", __func__);
  if (!crc16_check (send, 2, crc16[0], crc16[1])) {
    ac_warn ("%s (): load and lock secret: failed CRC (1st pass), issuing reset", __func__);
    // --- datasheet specifies that we must issue a reset here, so we reset
    // in drup.c if authenticator_generate_secret () fails.
    return false;
  }
  try (controller_write_byte, (ctrl, RELEASE_BYTE));
  nanosleep (&T_PRS, NULL);
  try (controller_read_byte, (ctrl, &cs));
  if (cs != 0xaa)
    ac_warn_rf ("%s (): load and lock secret: programming failed", __func__);

  uint8_t zero[32];
  memset (zero, 0x00, 32);
  try (write_scratchpad, (transaction, zero, true));

  return true;
}

static bool validate_page_num (uint8_t page_num) {
  if (page_num != 0 && page_num != 1)
    ac_warn_rf ("invalid page_num %d", page_num);
  return true;
}

static bool write_memory_segment (transaction_t transaction, uint8_t const page_num, uint8_t const seg_num, const uint8_t * const data) {
  drup_controller_t_const ctrl = transaction->ctrl;
  try_with_info (validate_page_num, (page_num), ac_warn, "write_memory_segment ()");
  try (transaction_prepare_device_command, (transaction, 0));
  uint8_t send[] = {
    COMMAND_WRITE_MEMORY,
    page_num | (seg_num << 5),
  };
  uint8_t crc16[2];
  uint8_t cs;
  try (controller_write_bytes, (ctrl, send, 2));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (crc16[0] == 0xff && crc16[1] == 0xff)
    ac_warn_rf ("%s (): segment %d, bad param", __func__, seg_num);
  if (!crc16_check (send, 2, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): segment %d, failed CRC (1st pass)", __func__, seg_num);
  try (controller_write_bytes, (ctrl, data, 4));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (!crc16_check (data, 4, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): segment %d, failed CRC (2nd pass)", __func__, seg_num);
  try (controller_write_byte, (ctrl, RELEASE_BYTE));
  nanosleep (&T_PRD, NULL);
  try_info (controller_read_byte, (ctrl, &cs), "seg num %d", seg_num);
  if (cs != 0xaa)
    ac_warn_rf ("%s (): segment %d, write failed", __func__, seg_num);
  return true;
}

static bool write_memory_full_page (transaction_t transaction, uint8_t page_num, const uint8_t * const data) {
  for (uint8_t seg_num = 0; seg_num < 8; seg_num++)
    try (write_memory_segment, (transaction, page_num, seg_num, data + (seg_num*4)));
  return true;
}

static bool write_memory_random (transaction_t transaction, uint8_t page_num, uint8_t *ret_data) {
  uint8_t data[32];
  bool ok = rand_bytes (data, 32);
  if (!ok) ac_piep_rf;
  try (write_memory_full_page, (transaction, page_num, data));
  memcpy (ret_data, data, 32);
  return true;
}

static bool read_memory_full_page (transaction_t transaction, uint8_t const page_num, uint8_t *ret_data) {
  drup_controller_t_const ctrl = transaction->ctrl;
  try_with_info (validate_page_num, (page_num), ac_warn, "read_memory_full_page ()");
  uint8_t send[] = {
    COMMAND_READ_MEMORY,
    page_num | (0 << 5),
  };
  uint8_t crc16[2];
  uint8_t data[32];
  try (controller_write_bytes, (ctrl, send, 2));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (crc16[0] == 0xff && crc16[1] == 0xff)
    ac_warn_rf ("%s (): bad param", __func__);
  if (!crc16_check (send, 2, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): failed CRC (1st pass)", __func__);
  try (controller_read_bytes, (ctrl, data, 32));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (!crc16_check (data, 32, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): failed CRC (2nd pass)", __func__);
  if (ret_data != NULL) memcpy (ret_data, data, 32);
  return true;
}

static bool write_challenge_to_scratchpad (transaction_t transaction, uint8_t *ret_data) {
  uint8_t challenge[32];
  bool ok = rand_bytes (challenge, 32);
  if (!ok) ac_piep_rf;
  try (write_scratchpad, (transaction, challenge, false));
  memcpy (ret_data, challenge, 32);
  return true;
}

static bool compute_and_read_mac (transaction_t transaction, uint8_t const page_num, uint8_t *ret_mac) {
  drup_controller_t_const ctrl = transaction->ctrl;
  try_with_info (validate_page_num, (page_num), ac_warn, "compute_and_read_mac ()");
  uint8_t send[] = {
    COMMAND_COMPUTE_AND_READ_MAC,
    page_num,
  };
  uint8_t crc16[2];
  uint8_t data[32];
  uint8_t cs;
  try (controller_write_bytes, (ctrl, send, 2));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (crc16[0] == 0xff && crc16[1] == 0xff)
    ac_warn_rf ("%s (): bad param", __func__);
  if (!crc16_check (send, 2, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): failed CRC (1st pass)", __func__);
  nanosleep (&T_CSHA, NULL);
  nanosleep (&T_CSHA, NULL);
  try (controller_read_byte, (ctrl, &cs));
  if (cs != 0xaa)
    ac_warn_rf ("%s (): MAC computation failed", __func__);
  try (controller_read_bytes, (ctrl, data, 32));
  try (controller_read_bytes, (ctrl, crc16, 2));
  if (!crc16_check (data, 32, crc16[0], crc16[1]))
    ac_warn_rf ("%s (): failed CRC (2nd pass)", __func__);
  memcpy (ret_mac, data, 32);
  return true;
}

static void make_mac (
    const uint8_t *page_contents, const uint8_t *challenge,
    const uint8_t *secret, const uint8_t *rom,
    uint8_t page_num, uint8_t man_id_h, uint8_t man_id_l, uint8_t *ret
) {
  const uint8_t *pp = page_contents;
  const uint8_t *ch = challenge;
  const uint8_t *ss = secret;
  const uint8_t *rn = rom;
  uint8_t input[128] = {
    pp[0x03], pp[0x02], pp[0x01], pp[0x00],
    pp[0x07], pp[0x06], pp[0x05], pp[0x04],
    pp[0x0b], pp[0x0a], pp[0x09], pp[0x08],
    pp[0x0f], pp[0x0e], pp[0x0d], pp[0x0c],
    pp[0x13], pp[0x12], pp[0x11], pp[0x10],
    pp[0x17], pp[0x16], pp[0x15], pp[0x14],
    pp[0x1b], pp[0x1a], pp[0x19], pp[0x18],
    pp[0x1f], pp[0x1e], pp[0x1d], pp[0x1c],
    ch[0x03], ch[0x02], ch[0x01], ch[0x00],
    ch[0x07], ch[0x06], ch[0x05], ch[0x04],
    ch[0x0b], ch[0x0a], ch[0x09], ch[0x08],
    ch[0x0f], ch[0x0e], ch[0x0d], ch[0x0c],
    ch[0x13], ch[0x12], ch[0x11], ch[0x10],
    ch[0x17], ch[0x16], ch[0x15], ch[0x14],
    ch[0x1b], ch[0x1a], ch[0x19], ch[0x18],
    ch[0x1f], ch[0x1e], ch[0x1d], ch[0x1c],
    ss[0x03], ss[0x02], ss[0x01], ss[0x00],
    ss[0x07], ss[0x06], ss[0x05], ss[0x04],
    ss[0x0b], ss[0x0a], ss[0x09], ss[0x08],
    ss[0x0f], ss[0x0e], ss[0x0d], ss[0x0c],
    ss[0x13], ss[0x12], ss[0x11], ss[0x10],
    ss[0x17], ss[0x16], ss[0x15], ss[0x14],
    ss[0x1b], ss[0x1a], ss[0x19], ss[0x18],
    ss[0x1f], ss[0x1e], ss[0x1d], ss[0x1c],
    rn[0x03], rn[0x02], rn[0x01], rn[0x00],
    rn[0x07], rn[0x06], rn[0x05], rn[0x04],
    0x00,     page_num, man_id_h, man_id_l,
    0x00,     0x00,     0x00,     0x00,
    0x00,     0x00,     0x00,     0x00,
    // --- the last 9 bytes are not used, but are specified in the data
    // sheet for some reason.
    0x00,     0x00,     0x00,     0x80,
    0x00,     0x00,     0x00,     0x00,
    0x00,     0x00,     0x03,     0xb8,
  };
  debug_buf (input, uint8_t, 128, 4, "input");
  compute_mac_sha256_maxim (input, ret);
}

static bool generate_secret (gensecret_transaction_t gensecret_transaction, uint8_t *secret) {
  transaction_t transaction = &gensecret_transaction->base;
  bool ok = rand_bytes (secret, 32);
  if (!ok) ac_piep_rf;
  try (write_scratchpad, (transaction, secret, true));
  try (transaction_prepare_device_command, (transaction, 0));
  try (read_scratchpad, (transaction, NULL));
  try (transaction_prepare_device_command, (transaction, 0));
  try (load_and_lock, (transaction));
  return true;
}

static bool auth_attempt (auth_transaction_t auth_transaction, bool *authenticated) {
  transaction_t transaction = &auth_transaction->base;
  uint8_t page_contents[32];
  try (write_memory_random, (transaction, AUTH_PAGE_NUM, page_contents));
  try (transaction_prepare_device_command, (transaction, 0));
  try (read_memory_full_page, (transaction, AUTH_PAGE_NUM, NULL));

  uint8_t challenge[32];
  try (transaction_prepare_device_command, (transaction, 0));
  try (write_challenge_to_scratchpad, (transaction, challenge));

  uint8_t man_id_l = 0x0;
  uint8_t man_id_h = 0x0;

  uint8_t mac_device[32];
  try (transaction_prepare_device_command, (transaction, 0));
  try (compute_and_read_mac, (transaction, AUTH_PAGE_NUM, mac_device));

  uint8_t mac_host[32];
  make_mac (page_contents, challenge, auth_transaction->secret, transaction->device_id, 0x01, man_id_h, man_id_l, mac_host);

  debug_buf (auth_transaction->secret, uint8_t, 32, 4, "attempt secret");
  debug_buf (transaction->device_id, uint8_t, 8, 4, "attempt device_id");

  debug_mac ("host", mac_host);
  debug_mac ("device", mac_device);

  if (!memcmp (mac_host, mac_device, 32))
    *authenticated = true;
  return true;
}

bool ds1964_generate_secret (gensecret_transaction_t gensecret_transaction, uint8_t *secret) {
  retry (5, GENSECRET_RETRY_MS, generate_secret, (gensecret_transaction, secret));
  return true;
}

bool ds1964_auth_attempt (auth_transaction_t auth_transaction, bool *authenticated) {
  retry (5, AUTH_RETRY_MS, auth_attempt, (auth_transaction, authenticated));
  return true;
}


#pragma GCC diagnostic pop
