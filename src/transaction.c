#include <alleycat.h>

#include "internal/transaction.h"

gensecret_transaction_t gensecret_transaction_mk (drup_controller_t_const ctrl, const uint8_t *device_id) {
  ac_try_malloc_rnull (gensecret_transaction_t, t)
  t->base.ctrl = ctrl;
  t->base.device_id = device_id;
  return t;
}

auth_transaction_t auth_transaction_mk (drup_controller_t_const ctrl, const uint8_t *device_id, const uint8_t *secret) {
  ac_try_malloc_rnull (auth_transaction_t, t)
  t->base.ctrl = ctrl;
  t->base.device_id = device_id;
  t->secret = secret;
  return t;
}

void auth_transaction_destroy (auth_transaction_t t) {
  free (t);
}

void gensecret_transaction_destroy (gensecret_transaction_t t) {
  free (t);
}

bool transaction_prepare_device_command (transaction_t transaction, uint8_t command) {
  return controller_prepare_device_command (transaction->ctrl, transaction->device_id, command);
}
