#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <i2c-dev-linux.h>
#include <alleycat.h>

#include "drup.h"

#include "internal/authenticator.h"
#include "internal/controller.h"
#include "internal/drup.h"
#include "internal/ds2477.h"
#include "internal/ds2482.h"
#include "internal/util.h"

// --- return value needs to be freed by caller
static struct controller_spec *get_spec (controller_type_t type) {
  ac_try_malloc_rnull (struct controller_spec *, t)
  if (type == DS2477) *t = (struct controller_spec) {
    .name = "DS2477",
    .num_buses = 1,
  };
  else if (type == DS2482_100) *t = (struct controller_spec) {
    .name = "DS2482-100",
    .num_buses = 1,
  };
  else if (type == DS2482_800) *t = (struct controller_spec) {
    .name = "DS2482-800",
    .num_buses = 8,
  };
  else ac_err_rn ("[internal error]: controller.c: invalid type: %d", type);
  return t;
}

static struct controller_config *make_config (const char *i2c_dev, uint8_t i2c_address) {
  ac_try_malloc_rnull (struct controller_config *, t)
  *t = (struct controller_config) {
    .i2c_dev = i2c_dev,
    .i2c_address = i2c_address,
  };
  return t;
}

/**
 * Note that this will open several file descriptors on a single underlying
 * device (e.g. /dev/i2c-1) if several controllers want to use it
 * simultaneously.
 *
 * Each controller keeps track of its own `fd` value.
 */

static bool init_io (const char *i2c_dev, int *fd, bool mock) {
  if (mock) {
    *fd = 0;
    ac_info ("[mock] opening device at %s in read-write mode", i2c_dev);
  }
  else {
    *fd = open (i2c_dev, O_RDWR);
    if (*fd == -1) ac_warn_perr_rf (
      "Couldn't open device %s", i2c_dev
    );
  }
  return true;
}

bool controller_init (drup_controller_t_const t, bool mock) {
  uint8_t i2c_address = t->config->i2c_address;
  if (mock) {
    ac_info ("[mock] initializing controller at address 0x%02x as I2C peripheral", i2c_address);
    ac_info ("[mock] resetting controller at address 0x%02x", i2c_address);
  }
  else {
    if (ioctl (t->fd, I2C_SLAVE, i2c_address))
      ac_err_perr_rf ("Unable to initialize controller at address 0x%02x as I2C peripheral", i2c_address);
    if (!controller_device_reset (t)) ac_err_perr_rf (
      "Controller at address 0x%02x not responding", i2c_address
    );
  }
  return true;
}

drup_controller_t drup_controller_mk (controller_type_t type, const char *i2c_dev, uint8_t i2c_address, bool mock) {
  struct controller_spec *spec = get_spec (type);
  if (spec == NULL) ac_piep_rn;
  struct controller_config *config = make_config (i2c_dev, i2c_address);
  if (config == NULL) ac_piep_rn;

  ac_try_malloc_rnull (struct drup_controller_impl *, t)

  t->type = type;
  t->spec = spec;
  t->config = config;

  uint8_t num_buses = t->spec->num_buses;
  ac_try_calloc_rnull (struct bus **, buses, num_buses)
  for (uint8_t i = 0; i < num_buses; i++) {
    buses[i] = bus_new (t, i);
    if (buses[i] == NULL) ac_piep_rn;
  }
  t->buses = buses;
  t->selected_bus = NULL;

  int fd;
  if (!init_io (i2c_dev, &fd, mock))
    ac_piep_rn;
  t->fd = fd;

  return t;
}

void drup_controller_destroy (drup_controller_t t) {
  close (t->fd);
  for (int i = 0; i < t->spec->num_buses; i++)
    bus_destroy (t->buses[i]);
  free (t->buses);
  free ((struct controller_spec *) t->spec);
  free ((struct controller_config *) t->config);
  free (t);
}

struct drup_controller_view drup_controller_view (drup_controller_t_const t) {
  struct drup_controller_view info = {
    .name = t->spec->name,
    .type = t->type,
    .i2c_dev = t->config->i2c_dev,
    .i2c_address = t->config->i2c_address,
    .num_buses = t->spec->num_buses,
  };
  return info;
}

struct ow_config *controller_get_selected_bus_ow_config (drup_controller_t_const t) {
  const struct bus *bus = t->selected_bus;
  if (bus == NULL) ac_warn_rn ("%s: no selected bus yet", __func__);
  return bus->ow_config;
}

uint8_t drup_controller_num_buses (drup_controller_t_const t) {
  return t->spec->num_buses;
}

// ------ abstract functions which dispatch to the specific implementations.

#define dispatch(ctrl, f_ds2477, f_ds2482_100, f_ds2482_800, args) do { \
  if (ctrl->type == DS2477)     return f_ds2477 args; \
  if (ctrl->type == DS2482_100) return f_ds2482_100 args; \
  if (ctrl->type == DS2482_800) return f_ds2482_800 args; \
  ac_piep_rf; \
} while (0)

bool controller_device_reset (drup_controller_t_const ctrl) {
  dispatch (ctrl, ds2477_device_reset, ds2482_device_reset, ds2482_device_reset, (ctrl));
}

bool controller_search_init (drup_controller_t ctrl, const struct bus *bus, const struct ow_config *cfg, presence_t *presence) {
  dispatch (ctrl, ds2477_search_init, ds2482_search_init, ds2482_search_init, (ctrl, bus, cfg, presence));
}

bool controller_search (
    drup_controller_t_const ctrl, drup_authenticator_t_const auth, uint8_t max_results,
    uint8_t device_ids[][8], uint8_t *num_results
) {
  dispatch (ctrl, ds2477_search_start, ds2482_search_start, ds2482_search_start, (ctrl, auth, max_results, device_ids, num_results));
}

bool controller_w1_reset_external (drup_controller_t_const ctrl) {
  dispatch (ctrl, ds2477_w1_reset_external, ds2482_w1_reset_external, ds2482_w1_reset_external, (ctrl));
}

bool controller_w1_reset (drup_controller_t_const ctrl, presence_t *presence) {
  dispatch (ctrl, ds2477_w1_reset, ds2482_w1_reset, ds2482_w1_reset, (ctrl, presence));
}

bool controller_write_bytes (drup_controller_t_const ctrl, const uint8_t *buf, uint8_t cnt) {
  dispatch (ctrl, ds2477_write_bytes, ds2482_write_bytes, ds2482_write_bytes, (ctrl, buf, cnt));
}

bool controller_write_byte (drup_controller_t_const ctrl, uint8_t b) {
  dispatch (ctrl, ds2477_write_byte, ds2482_write_byte, ds2482_write_byte, (ctrl, b));
}

bool controller_read_bytes (drup_controller_t_const ctrl, uint8_t *buf, uint8_t cnt) {
  dispatch (ctrl, ds2477_read_bytes, ds2482_read_bytes, ds2482_read_bytes, (ctrl, buf, cnt));
}

bool controller_read_byte (drup_controller_t_const ctrl, uint8_t *b) {
  dispatch (ctrl, ds2477_read_byte, ds2482_read_byte, ds2482_read_byte, (ctrl, b));
}

bool controller_prepare_device_command (drup_controller_t_const ctrl, const uint8_t *device_id, uint8_t command) {
  dispatch (ctrl, ds2477_prepare_device_command, ds2482_prepare_device_command, ds2482_prepare_device_command, (ctrl, device_id, command));
}
