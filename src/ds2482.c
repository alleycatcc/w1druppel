#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <alleycat.h>

#include <crc8-dow.h>

#include "internal/authenticator.h"
#include "internal/controller.h"
#include "internal/i2c.h"
#include "internal/util.h"

#include "internal/ds2482.h"

/* Datasheet seems to suggest that the rom command itself should be
 * sent at standard speed, even for setting overdrive. However this a)
 * doesn't seem to help and b) messes up the DS1964, which must always be
 * at overdrive. Keep this at false for now.
 */
#define ROM_COMMAND_AT_STANDARD_SPEED false

/*
 * Note, SPU only applies for a single one-wire operation. The devices call
 * the prepare transaction function before every one-wire operation, which calls rom_command (), which ends with SPU being set, so in principle SPU should be on at all the points it's needed.
 */

/* | bit 7 | bit 6 | bit 5 | bit 4 | bit 3 | bit 2 | bit 1 | bit 0
 *   ------ ------- ------- ------- ------- ------- ------- ------
 *    ---     ---             ---
 *    1WS     SPU      1      APU     1WS     SPU      0      APU */

static uint8_t prepare_config_param (uint8_t cfg) {
  uint8_t ncfg = ~cfg;
  return cfg | (ncfg << 4);
}

static uint8_t mk_config_param (const struct ow_config *cfg, bool spu) {
  uint8_t val = 0
    | (cfg->overdrive ? CFG_REG_1WS : 0)
    | (cfg->active_pullup ? CFG_REG_APU : 0)
    | (spu ? CFG_REG_SPU : 0);
  return prepare_config_param (val);
}

// --- note: An old datasheet from Maxim says: "The overdrive speed is
// intended for use only on very short connections and is never suitable for
// use in 1-Wired networks." This is probably not true any more (DS1964 can
// only be used at overdrive speed for example).

static bool search_triplets (drup_controller_t_const ctrl, uint8_t device_family, bool *read, uint8_t * const ret);

static bool configure (drup_controller_t_const ctrl, uint8_t cfg) {
  try_info (i2c_cmd_with_data, (ctrl, CMD_WRITE_CONFIG, cfg), "CMD_WRITE_CONFIG");
  return true;
}

static bool select_register (drup_controller_t_const ctrl, uint8_t reg) {
  try_info (i2c_cmd_with_data, (ctrl, CMD_SET_READ_PTR, reg), "CMD_SET_READ_PTR");
  return true;
}

static bool get_current_config (drup_controller_t_const ctrl, uint8_t *cfg) {
  try_info (select_register, (ctrl, SELECT_CODE_CONFIG), "SELECT_CODE_CONFIG");
  try (i2c_read_byte, (ctrl, cfg));
  return true;
}

static bool wait_idle (drup_controller_t_const ctrl, uint8_t *status) {
  uint8_t val;
  try_info (select_register, (ctrl, SELECT_CODE_STATUS), "SELECT_CODE_STATUS");
  do try (i2c_read_byte, (ctrl, &val));
  while (val & STATUS_BUSY);
  if (status != NULL) *status = val;
  return true;
}

static bool touch_bit (drup_controller_t_const ctrl, bool const bit, bool * const ret) {
  uint8_t status;
  try (i2c_cmd_with_data, (ctrl, CMD_W1_SINGLE_BIT, bit ? 0x80 : 0));
  try (wait_idle, (ctrl, &status));
  if (ret != NULL) *ret = (status & STATUS_SINGLE_BIT_READ) != 0;
  return true;
}

/**
 * Sends CMD_W1_RESET to devices on the selected bus and returns presence in
 * `presence`. Set to NULL to ignore presence.
 *
 * Not to be confused with CMD_DEVICE_RESET, which initialises the
 * controller, and is only used at the very beginning and doesn't have its
 * own function.
 *
 * Exported, because some devices need to be able to do this at certain
 * points.
 *
 * Be sure to turn SPU off before calling this.
 *
 *     "Note: Strong pullup also affects the 1-Wire Reset command. If
 *     enabled, it can cause incorrect reading of the presence pulse and may
 *     cause a violation of the device's absolute maximum rating."
 *
 * If in doubt, use ds2482_w1_reset_external, which explicitly turns off
 * SPU.
 */

bool ds2482_w1_reset (drup_controller_t_const ctrl, presence_t *presence) {
  uint8_t read;
  try (wait_idle, (ctrl, NULL));
  try_info (i2c_cmd, (ctrl, CMD_W1_RESET), "CMD_W1_RESET");
  try (wait_idle, (ctrl, &read));

  /* Presence means a key is making contact with the probe. If it's crooked
   * on the edge we return PRESENCE_WITH_SHORT (in kattenluik we turn the
   * led to yellow on this event). PRESENCE_READY means you can initiate the
   * search / authenticate / etc.
   */

  if (presence != NULL) {
    if (!(read & STATUS_PRESENCE_PULSE_DETECT)) *presence = PRESENCE_NONE;
    else if (read & STATUS_SHORT_DETECTED) *presence = PRESENCE_WITH_SHORT;
    else *presence = PRESENCE_READY;
  }
  return true;
}

/* For when an external module (e.g. drup.c) wants the controller to reset.
 * It's a separate function so that we are sure to disable SPU.
 */
bool ds2482_w1_reset_external (drup_controller_t_const ctrl) {
  uint8_t cfg;
  try (get_current_config, (ctrl, &cfg));
  try (configure, (ctrl, prepare_config_param (cfg & ~CFG_REG_SPU)));
  try (ds2482_w1_reset, (ctrl, NULL));
  return true;
}

static bool _write_configuration (drup_controller_t_const ctrl, const struct ow_config *cfg, bool spu) {
  uint8_t cfg_param = mk_config_param (cfg, spu);
  try (configure, (ctrl, cfg_param));
  return true;
}

static bool cmd_triplet (drup_controller_t_const ctrl, uint8_t dir, uint8_t *triplet) {
  uint8_t status;
  try_info (i2c_cmd_with_data, (ctrl, CMD_W1_TRIPLET, dir ? 0xFF : 0), "CMD_W1_TRIPLET");
  try (wait_idle, (ctrl, &status));
  *triplet = status >> 5;
  return true;
}

static bool bus_select (drup_controller_t ctrl, const struct bus *bus) {
  uint8_t bus_num = bus->number;
  try_with_info (i2c_cmd_with_data, (ctrl, CMD_CHANNEL_SELECT, CHANS_SELECT[bus_num]), ac_warn_perr, "CMD_CHANNEL_SELECT");
  uint8_t ack = 0;
  try (i2c_read_byte, (ctrl, &ack));
  if (ack != CHANS_SELECT_ACK [bus_num])
    ac_warn_rf ("Channel select command succeeded but channel not selected");
  ctrl->selected_bus = bus;
  return true;
}

// --- configure, spu off
bool write_configuration (drup_controller_t_const ctrl, const struct ow_config *cfg) {
  return _write_configuration (ctrl, cfg, false);
}

// --- configure, spu on
static bool write_configuration_spu_on (drup_controller_t_const ctrl, const struct ow_config *cfg) {
  return _write_configuration (ctrl, cfg, true);
}

bool ds2482_write_byte (drup_controller_t_const ctrl, uint8_t byte) {
  try (i2c_cmd_with_data, (ctrl, CMD_W1_WRITE_BYTE, byte));
  try (wait_idle, (ctrl, NULL));
  return true;
}

bool ds2482_device_reset (drup_controller_t_const ctrl) {
  try_info (i2c_cmd, (ctrl, CMD_DEVICE_RESET), "CMD_DEVICE_RESET");
  return true;
}

bool ds2482_write_bytes (drup_controller_t_const ctrl, const uint8_t *bytes, uint8_t count) {
  for (int i = 0; i < count; i++)
    try (ds2482_write_byte, (ctrl, bytes[i]));
  return true;
}

bool ds2482_read_byte (drup_controller_t_const ctrl, uint8_t *read) {
  try (i2c_cmd, (ctrl, CMD_W1_READ_BYTE));
  try (wait_idle, (ctrl, NULL));
  try (select_register, (ctrl, SELECT_CODE_DATA));
  try (i2c_read_byte, (ctrl, read));
  return true;
}

bool ds2482_read_bit (drup_controller_t_const ctrl, bool * const ret) {
  try (touch_bit, (ctrl, 1, ret));
  return true;
}

bool ds2482_write_bit (drup_controller_t_const ctrl, bool bit) {
  try (touch_bit, (ctrl, bit, NULL));
  return true;
}

bool ds2482_read_bytes (drup_controller_t_const ctrl, uint8_t *read, uint8_t count) {
  for (int i = 0; i < count; i++)
    try (ds2482_read_byte, (ctrl, read + i));
  return true;
}

// --- @todo check match rom cases

/* Setting overdrive on devices is pretty tricky. We haven't been able to
 * confirm higher speeds for the DS1961 (and the DS1964 is always
 * overdrive), which could be due to the order of operations in this
 * function not being exactly what the protocol expects.
 */
static bool rom_command (drup_controller_t_const ctrl, const uint8_t *device_id, struct ow_config *cfg) {
  // --- ensure spu off
  try (write_configuration, (ctrl, cfg));
  try (ds2482_w1_reset, (ctrl, NULL));
  if (ROM_COMMAND_AT_STANDARD_SPEED) {
    struct ow_config cfg_no_overdrive = {
      .overdrive = false,
      .active_pullup = cfg->active_pullup,
    };
    try (write_configuration, (ctrl, &cfg_no_overdrive));
  }
  if (device_id == NULL) {
    if (cfg->overdrive)
      try_info (ds2482_write_byte, (ctrl, W1_SKIP_ROM_OVERDRIVE), "W1_SKIP_ROM_OVERDRIVE");
    else
      try_info (ds2482_write_byte, (ctrl, W1_SKIP_ROM), "W1_SKIP_ROM");
  }
  else {
    if (cfg->overdrive)
      try_info (ds2482_write_byte, (ctrl, W1_MATCH_ROM_OVERDRIVE), "W1_MATCH_ROM_OVERDRIVE");
    else
      try_info (ds2482_write_byte, (ctrl, W1_MATCH_ROM), "W1_MATCH_ROM");
  }
  try (wait_idle, (ctrl, NULL));
  if (ROM_COMMAND_AT_STANDARD_SPEED && cfg->overdrive)
    try (write_configuration, (ctrl, cfg));
  // --- datasheets are a bit ambiguous about whether these bytes should be
  // before or after setting the DS2482 config. Here we choose after.
  if (device_id != NULL) for (int i = 0; i < 8; i++) {
    try_info (ds2482_write_byte, (ctrl, device_id[i]), "W1_MATCH_ROM byte");
    try (wait_idle, (ctrl, NULL));
  }
  try (write_configuration_spu_on, (ctrl, cfg));
  return true;
}

static bool is_single_drop (drup_controller_t_const ctrl) {
  const network_type_t network_type = ctrl->selected_bus->network_type;
  return network_type == SINGLE_DROP;
}

bool ds2482_prepare_device_command (drup_controller_t_const ctrl, const uint8_t *device_id, uint8_t command) {
  struct ow_config *cfg = controller_get_selected_bus_ow_config (ctrl);
  if (cfg == NULL) ac_warn_rf ("%s: Couldn't get current onewire config", __func__);
  if (is_single_drop (ctrl)) try (rom_command, (ctrl, NULL, cfg));
  else try (rom_command, (ctrl, device_id, cfg));
  if (command != 0) {
    try (ds2482_write_byte, (ctrl, command));
    try (wait_idle, (ctrl, NULL));
  }
  return true;
}

static bool read_rom (drup_controller_t_const ctrl, uint8_t device_family, bool *read, uint8_t *device_id) {
  *read = false;
  try (i2c_cmd_with_data, (ctrl, CMD_W1_WRITE_BYTE, W1_READ_ROM));
  try (wait_idle, (ctrl, NULL));
  try (select_register, (ctrl, SELECT_CODE_DATA));

  for (uint8_t i = 0; i < 8; i++) {
    uint8_t b;
    try (ds2482_read_byte, (ctrl, &b));
    device_id[i] = b;
  }

  if (!crc8_ok (device_id, 8))
    ac_warn_rf ("%s (): got invalid device ID (bad read or no key present)", __func__);

  uint8_t device_family_got = device_id[0];
  if (device_family != 0 && device_family != device_family_got) {
    ac_info ("ignoring device with family 0x%02x (wanted 0x%02x)", device_family_got, device_family);
    return true;
  }
  *read = true;
  return true;
}

/* Select the given bus and prepare for search.
 * Returns whether a device was detected in &presence.
 */
bool ds2482_search_init (drup_controller_t ctrl, const struct bus *bus, const struct ow_config *cfg, presence_t *presence) {
  try (write_configuration, (ctrl, cfg));
  try (bus_select, (ctrl, bus));
  try (ds2482_w1_reset, (ctrl, presence));
  return true;
}

/* @todo implement multi-search (triplet search returns max 1 currently)
 */
bool ds2482_search_start (drup_controller_t_const ctrl, drup_authenticator_t_const auth, uint8_t max_results, uint8_t device_ids[][8], uint8_t *num_results) {
  if (max_results == 0) ac_piep_rf;
  bool do_triplets = !is_single_drop (ctrl);
  uint8_t device_family_want = auth->spec->device_family;
  bool read = false;
  if (do_triplets) try (search_triplets, (ctrl, device_family_want, &read, device_ids[0]));
  else try (read_rom, (ctrl, device_family_want, &read, device_ids[0]));
  *num_results = read ? 1 : 0;
  return true;
}

static int triplet_determine_bit (int const desc_bit, int const bit) {
  int prev_device_id = 0;
  if      (bit == desc_bit) return 1;
  else if (bit >  desc_bit) return 0;
  else return ((prev_device_id >> bit) & 0x1);
}

static bool triplet_some_device_responded (uint8_t triplet) {
  return (triplet & 0x03) != 0x03;
}

static void bits_reversed (bool *tgt, uint8_t src) {
  for (uint8_t i = 0; i < 8; i++)
    tgt[i] = (bool) (src & (1 << i));
}

/**
 * `device_family` is an optional hex value to filter for a specific device
 * family. Set to 0 to disable filtering.
 */

static bool search_triplets (drup_controller_t_const ctrl, uint8_t device_family, bool *read, uint8_t * const device_id) {
  *read = false;
  int desc_bit = 64;
  int bit_num = -1;
  int byte_num = -1;

  bool family_reversed[8];
  if (device_family) bits_reversed (family_reversed, device_family);

  try (i2c_cmd_with_data, (ctrl, CMD_W1_WRITE_BYTE, W1_SEARCH_ALL));
  try (wait_idle, (ctrl, NULL));

  bool broke = false;
  // --- each bit ~ 3ms, all I/O
  while (++bit_num < 64) {
    uint8_t triplet;
    uint8_t search_bit = triplet_determine_bit (desc_bit, bit_num);
    try (cmd_triplet, (ctrl, search_bit, &triplet));

    if (!triplet_some_device_responded (triplet)) {
      ac_debug ("Quitting at bit %d", bit_num);
      broke = true;
      break;
    }

    uint64_t bit_val = triplet >> 2;
    if (bit_num % 8 == 0) {
      byte_num++;
      device_id[byte_num] = 0;
    }
    device_id[byte_num] |= (bit_val << bit_num) >> (byte_num * 8);

    if (device_family) {
      if (bit_num < 8 && family_reversed[bit_num] != (bool) bit_val) {
        ac_info ("ignoring device with family != 0x%02x", device_family);
        ac_debug ("bit num = %d, expected = %d, bit val = %ld\n", bit_num, family_reversed[bit_num], bit_val);
        broke = true;
        break;
      }
    }
  }

  if (!broke) *read = true;
  return true;
}

/* Reference implementation of the triplet search provided by Maxim */

#ifdef SEARCH_TYPE_MAXIM
static bool search_triplets (drup_controller_t_const ctrl, uint64_t * const device_id) {
  *device_id = 0;

  unsigned char serial_rom[8] = {0};
  uint64_t serial_rom64 = 0;

  bool search_direction;
  int rom_bit_number = 1;
  int rom_byte_number = 0;
  unsigned char rom_byte_mask = 1;
  int search_result = 0;

  do {
    bool id_bit;
    bool cmp_id_bit;
    try (ds2482_read_bit, (ctrl, &id_bit));
    try (ds2482_read_bit, (ctrl, &cmp_id_bit));

    if (id_bit && cmp_id_bit) break;
    if (id_bit != cmp_id_bit) search_direction = id_bit;
    else search_direction = false;

    if (search_direction) serial_rom[rom_byte_number] |= rom_byte_mask;
    else serial_rom[rom_byte_number] &= ~rom_byte_mask;

    try (ds2482_write_bit, (ctrl, (search_direction)));

    rom_bit_number++;
    rom_byte_mask <<= 1;

    serial_rom64 |= ((uint64_t) serial_rom[rom_byte_number]) << (rom_byte_number * 8);

    if (rom_byte_mask == 0) {
      rom_byte_number++;
      rom_byte_mask = 1;
    }
  } while (rom_byte_number < 8);

  if (rom_bit_number >= 65) {
    search_result = true;
    *device_id = serial_rom64;
  }

  if (!search_result || !serial_rom[0]) search_result = false;

  return true;
}
#endif
