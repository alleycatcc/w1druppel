#include <stddef.h>

#include <alleycat.h>

#include "internal/bus.h"
#include "internal/i2c.h"
#include "internal/drup.h"
#include "internal/util.h"

/* The configuration as requested by the user for a search/auth/secret
 * transaction. Doesn't get stored in the bus struct.
 */

// struct bus_ow_user_config {
  // bool overdrive;
  // active_pullup_type_t active_pullup_type;
// };

struct bus *bus_new (drup_controller_t ctrl, uint8_t bus_num) {
  if (bus_num >= 8) ac_warn_rn (
    "Invalid bus number %d", bus_num
  );
  ac_try_malloc_rnull (struct bus *, bus);
  ac_try_malloc_rnull (struct ow_config *, config)
  bus->controller = ctrl;
  bus->number = bus_num;
  bus->ow_config = config;
  return bus;
}

void bus_destroy (struct bus *bus) {
  free (bus->ow_config);
  free (bus);
}
