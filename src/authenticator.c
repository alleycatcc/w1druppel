#include <string.h>
#include <sys/param.h>

#include <alleycat.h>

#include "drup.h"

#include "internal/authenticator.h"
#include "internal/ds1961.h"
#include "internal/ds1964.h"
#include "internal/util.h"

static int secret_length_ds1961 = 8;
static int secret_length_ds1964 = 32;

// --- caller should free
static struct authenticator_spec *get_spec (authenticator_type_t type) {
  ac_try_malloc_rnull (struct authenticator_spec *, t)
  if (type == DS1961) {
    t->name = "DS1961";
    t->device_family = 0x33;
    t->overdrive_default = false;
    t->disable_overdrive_during_init = true;
    t->secret_length = secret_length_ds1961;
  }
  else if (type == DS1964) {
    t->name = "DS1964";
    t->device_family = 0xe2;
    t->overdrive_default = true;
    t->disable_overdrive_during_init = false;
    t->secret_length = secret_length_ds1964;
  }
  else ac_err_rn ("[internal error]: authenticator.c: Invalid type: %d", type);
  return t;
}

drup_authenticator_t drup_authenticator_mk (authenticator_type_t type) {
  if (type != DS1961 && type != DS1964) {
    ac_err ("[internal error]: drup_authenticator_mk (): Invalid type: %d", type);
    return NULL;
  }
  struct authenticator_spec *spec = get_spec (type);
  if (spec == NULL) ac_piep_rn;
  ac_try_malloc_rnull (struct drup_authenticator_impl *, t)

  t->type = type;
  t->spec = spec;
  return t;
}

void drup_authenticator_destroy (drup_authenticator_t t) {
  free ((struct authenticator_spec *) t->spec);
  free (t);
}

struct drup_authenticator_view drup_authenticator_view (drup_authenticator_t_const t) {
  const struct authenticator_spec *spec = t->spec;
  struct drup_authenticator_view view = {
    .name = spec->name,
    .type = t->type,
    .device_family = spec->device_family,
    .overdrive_default = spec->overdrive_default,
    .disable_overdrive_during_init = spec->disable_overdrive_during_init,
    .secret_length = spec->secret_length,
  };
  return view;
}

// ------ abstract functions which dispatch to the specific implementations.

// --- order: DS1961, DS1964
#define dispatch(auth, f_ds1961, f_ds1964, args) do { \
  if (auth->type == DS1961) return f_ds1961 args; \
  else if (auth->type == DS1964) return f_ds1964 args; \
  else ac_piep_rf; \
} while (0)

bool authenticator_attempt (drup_authenticator_t_const auth, auth_transaction_t transaction, bool *authenticated) {
  dispatch (auth, ds1961_auth_attempt, ds1964_auth_attempt, (transaction, authenticated));
}

bool authenticator_generate_secret (drup_authenticator_t_const auth, gensecret_transaction_t transaction, uint8_t *secret) {
  dispatch (auth, ds1961_generate_secret, ds1964_generate_secret, (transaction, secret));
}

/* Returns 0 on invalid auth_num */
uint8_t drup_get_secret_length_for_auth_num (drup_t_const drup, uint8_t auth_num) {
  struct drup_authenticator_view auth_view;
  if (!drup_authenticator_view_at (drup, auth_num, &auth_view))
    ac_piep_r0;
  return auth_view.secret_length;
}
