#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "internal/benchmark.h"

char last_tag[200];
char cur_tag[200];
char tag[200];
double last_time = 0;

struct timespec gtime;

#ifdef DO_BENCHMARK
static double get_time () {
  clock_gettime (CLOCK_REALTIME, &gtime);
  return gtime.tv_sec * 1e3 + gtime.tv_nsec / 1e6;
}
#endif

void benchmark_start (const char * const tag __attribute__ ((unused)), ...) {
#ifdef DO_BENCHMARK
  va_list fmt;
  va_start (fmt, tag);
  vsnprintf (last_tag, 200, tag, fmt);
  va_end (fmt);
  last_time = get_time ();
#endif
}

void benchmark (const char * const tag __attribute__ ((unused)), ...) {
#ifdef DO_BENCHMARK
  double now = get_time ();
  va_list fmt;
  va_start (fmt, tag);
  vsnprintf (cur_tag, 200, tag, fmt);
  va_end (fmt);
  printf ("[benchmark] [%s -> %s] %.1f ms\n", last_tag, cur_tag, now - last_time);
  strncpy (last_tag, cur_tag, 200);
  last_time = now;
#endif
}
