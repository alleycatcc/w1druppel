#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <stdbool.h>
#include <stdint.h>

#include <alleycat.h>

#include "internal/ds2477.h"

bool ds2477_device_reset (drup_controller_t_const ctrl) {
  return false;
}

bool ds2477_write_byte (drup_controller_t_const ctrl, uint8_t byte) {
  return false;
}

bool ds2477_search_init (drup_controller_t_const ctrl, const struct bus * const bus, const struct ow_config *cfg, presence_t *presence) {
  return false;
}

bool ds2477_search_start (drup_controller_t_const ctrl, drup_authenticator_t_const auth, uint8_t max_results, uint8_t device_ids[][8], uint8_t *num_results) {
  return false;
}

bool ds2477_bus_select (drup_controller_t_const ctrl, const struct bus * const bus) {
  return false;
}

// --- configure, including enable spu
bool ds2477_write_configuration (drup_controller_t_const ctrl, struct ow_config *cfg) {
  return false;
}

bool ds2477_write_configuration_spu (drup_controller_t_const ctrl, struct ow_config *cfg) {
  return false;
}

bool ds2477_wait_idle (drup_controller_t_const ctrl, uint8_t *status) {
  return false;
}

bool ds2477_select_register (drup_controller_t_const ctrl, uint8_t reg) {
  return false;
}

bool ds2477_triplet (drup_controller_t_const ctrl, uint8_t dir, uint8_t *triplet) {
  return false;
}

bool ds2477_w1_reset (drup_controller_t_const ctrl, presence_t *presence) {
  return false;
}

bool ds2477_w1_reset_external (drup_controller_t_const ctrl) {
  return false;
}

// bool ds2477_key_command_init_match_rom (drup_controller_t_const ctrl, const struct key * const key) {
  // return false;
// }

bool ds2477_key_command_init_skip_rom (drup_controller_t_const ctrl, uint8_t const cfg) {
  return false;
}

bool ds2477_write_bytes (drup_controller_t_const ctrl, const uint8_t *bytes, uint8_t count) {
  return false;
}

bool ds2477_read_byte (drup_controller_t_const ctrl, uint8_t *read) {
  return false;
}

bool ds2477_read_bit (drup_controller_t_const ctrl, bool * const ret) {
  return false;
}

bool ds2477_write_bit (drup_controller_t_const ctrl, bool const bit) {
  return false;
}

bool ds2477_read_bytes (drup_controller_t_const ctrl, uint8_t *read, uint8_t count) {
  return false;
}

bool ds2477_prepare_device_command (drup_controller_t_const ctrl, const uint8_t *device_id, uint8_t command) {
  return false;
}

#pragma GCC diagnostic pop
