#define _DEFAULT_SOURCE 1

#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
// --- for random_arc4 functions
#include <bsd/stdlib.h>

#include <uthash.h>

#include <alleycat.h>

#include "drup.h"
#include "drup-util.h"
#include "internal/benchmark.h"
#include "internal/util-debug-buffer.h"

#define destroy_hash(table, elt_type, handle, elt_destroy) do { \
  elt_type *elt, *tmp$$; \
  HASH_ITER (handle, table, elt, tmp$$) { \
    HASH_DEL (table, elt); \
    elt_destroy (elt); \
  } \
} while (0)

#ifndef MOCK
#define MOCK false
#endif

#define AUTHENTICATOR_TYPE0 DS1961
#define AUTHENTICATOR_TYPE1 DS1964
#define I2C_ADDRESS_2482 0x18
#define I2C_ADDRESS_2477 0x23

#define ACTIVE_PULLUP_ARY (active_pullup_type_t []) { APU_FORCE_OFF, APU_FORCE_ON, APU_AUTO_NEW, APU_AUTO_OLD, }

// --- null pointer is correct for uthash
struct secret_elt *secrets = NULL;

struct secret_elt {
  uint64_t device_id;
  uint8_t *secret;
  uint8_t secret_length;
  // --- make hashable
  UT_hash_handle hh;
};

void secret_elt_destroy (struct secret_elt *t) {
  free (t->secret);
  free (t);
}

void destroy_secrets () {
  destroy_hash (secrets, struct secret_elt, hh, secret_elt_destroy);
}

// bool device_is_known (struct secret_elt *secrets, uint64_t device_id, uint8_t *secret) {
bool device_is_known (struct secret_elt *secrets, uint64_t device_id, uint8_t **secret, uint8_t *secret_length) {
  struct secret_elt *elt;
  HASH_FIND_INT (secrets, &device_id, elt);
  ac_debug ("%d", elt==NULL);
  if (elt == NULL) return false;
  *secret = elt->secret;
  // memcpy (secret, elt->secret, 32);
  *secret_length = elt->secret_length;
  return true;
}

bool rand_bool_arc4 () {
  return (bool) arc4random_uniform (2);
}

// --- [0, n)
uint8_t rand_uint8_arc4 (uint8_t n) {
  if (n == 0) {
    ac_warn ("%s: n of 0 is not valid", __func__);
    return 0;
  }
  return (uint8_t) arc4random_uniform (n);
}

const int SLEEP_LOOP = 5e4;
const int SLEEP_AFTER_ACTION = 1e6;

// --- it's just an int.
volatile sig_atomic_t sigint_received = 0;

static void cleanup (drup_t drup) {
  drup_destroy (drup);
  destroy_secrets ();
}

static void handle_sigint (int _signum __attribute__ ((unused))) {
  *&sigint_received = 1;
}

bool interact_with_device (drup_t_const drup, const char *info, struct secret_elt **secrets, uint8_t ctrl_num, uint8_t auth_num, uint8_t *device_id, unsigned char *device_id_str) {
  char infox[200];
  snprintf (infox, 200, "%s found device %s", info, device_id_str);
  uint8_t *secret;
  uint8_t secret_length;
  if (device_is_known (*secrets, drup_hex_to_uint64 (device_id_str), &secret, &secret_length)) {
    ac_info ("%s, authenticating with secret of length %d", infox, secret_length);
    debug_buf (secret, uint8_t, secret_length, 4, "test hash secret");
    bool authenticated = false;
    if (!drup_authenticate (drup, ctrl_num, auth_num, device_id, secret, &authenticated))
      ac_piep_rf;
    if (authenticated) ac_info ("%s ٭٭٭ cha-ching ٭٭٭", info);
    else ac_info ("%s ٭٭٭ nope ٭٭٭", info);
  }
  else {
    uint8_t secret_length = drup_get_secret_length_for_auth_num (drup, auth_num);
    if (secret_length == 0) ac_piep_rf;
    ac_info ("%s, generating secret of length %d", infox, secret_length);
    ac_try_calloc_die (uint8_t *, secret_gen, secret_length);
    if (!drup_generate_secret (drup, ctrl_num, auth_num, device_id, secret_gen))
      ac_piep_rf;
    ac_info ("secret generated");
    ac_try_malloc_die (struct secret_elt *, elt);
    *elt = (struct secret_elt) {
      .device_id = drup_hex_to_uint64 (device_id_str),
      .secret = secret_gen,
      .secret_length = secret_length,
    };
    HASH_ADD_INT (*secrets, device_id, elt);
  }
  return true;
}

bool main_loop (struct secret_elt **secrets, drup_t drup, uint8_t numc, uint8_t *num_buses_lookup, uint8_t numa, volatile sig_atomic_t *sigint_received) {
  if (*sigint_received) {
    ac_info ("doei");
    return false;
  }

  /* To keep the API simple, looping through controllers, then through the
   * channels of that controller, then through the authenticators, happens
   * here, instead of inside the drup functions.
   */

  for (uint8_t ctrl_num = 0; ctrl_num < numc; ctrl_num++) {
    uint8_t num_buses = num_buses_lookup[ctrl_num];
    for (uint8_t bus_num = 0; bus_num < num_buses; bus_num++) {
      for (uint8_t auth_num = 0; auth_num < numa; auth_num++) {
        char info[100];
        snprintf (info, 100, "[controller %d / bus %d / auth %d]", ctrl_num, bus_num, auth_num);
        presence_t presence;
        struct drup_bus_user_config ow_cfg = {
          .overdrive = OVERDRIVE_MOSTLY_ON,
          .active_pullup_type = ACTIVE_PULLUP_ARY [rand_uint8_arc4 (4)],
          .network_type = SINGLE_DROP,
        };
        benchmark_start ("search init");
        if (!drup_search_init (drup, ctrl_num, bus_num, auth_num, &ow_cfg, &presence)) {
          ac_piep;
          continue;
        }
        if (presence == PRESENCE_NONE) continue;
        if (presence == PRESENCE_WITH_SHORT) {
          printf ("[presence-with-short] ");
          continue;
        }
        if (presence == PRESENCE_READY) printf ("%s [presence-ready] ", info);
        else { ac_piep; continue; }

        uint8_t max_results = 3;
        uint8_t device_id[max_results][8];
        unsigned char device_id_str[max_results][17];
        uint8_t num_results;
        if (!drup_search (drup, ctrl_num, auth_num, max_results, device_id, device_id_str, &num_results)) {
          ac_piep;
          continue;
        }

        // ac_info ("Found, sleep 1");
        // sleep (1);

        for (uint8_t result = 0; result < num_results; result++) {
          if (!interact_with_device (drup, info, secrets, ctrl_num, auth_num, device_id[result], device_id_str[result])) {
            ac_piep;
            continue;
          }
        }

        usleep (SLEEP_AFTER_ACTION);
      }
    }
  }
  return true;
}

int main (void) {
  signal (2, handle_sigint);
  ac_autoflush (stdout);

  drup_controller_t ctrls[] = {
    drup_controller_mk (DS2482_800, "/dev/i2c-1", I2C_ADDRESS_2482, MOCK),
  };
  if (ctrls[0] == NULL) ac_piep_r1;
  drup_authenticator_t auths[] = {
    drup_authenticator_mk (AUTHENTICATOR_TYPE0),
    drup_authenticator_mk (AUTHENTICATOR_TYPE1),
  };
  if (auths[0] == NULL || auths[1] == NULL) ac_piep_r1;

  drup_t drup = drup_mk (ctrls, 1, auths, 2, true, MOCK);
  if (!drup) ac_die ("Couldn't init.");

  uint8_t num_buses_lookup[1] = {
    drup_controller_num_buses (ctrls[0]),
  };

  ac_info ("initted.");

  while (true) {
    bool reloop = main_loop (
      // --- must send double reference to hash table because otherwise
      // uthash modifies it and the result won't be passed in to the loop
      // again
      &secrets,
      drup, 1, num_buses_lookup, 2,
      &sigint_received
    );
    if (!reloop) break;
    usleep (SLEEP_LOOP);
  }

  cleanup (drup);
  return 0;
}
