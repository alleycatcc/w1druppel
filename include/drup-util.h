#ifndef _DRUP_UTIL_H
#define _DRUP_UTIL_H

void drup_hex_to_uint8 (const unsigned char *value, uint8_t *ret);
uint64_t drup_hex_to_uint64 (const unsigned char *value);
/* An older version produced 16 byte secrets for the DS1961, where even
 * numbered bytes ended up getting ignored, since it only needs 8 bytes.
 * This is here to still support these legacy secrets.
 */
void legacy_ds1961_secret_16_to_8 (uint8_t *tgt8, const uint8_t *src16);

#endif
