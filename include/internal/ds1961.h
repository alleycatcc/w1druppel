#ifndef _INTERNAL_DS1961_H
#define _INTERNAL_DS1961_H

#include <time.h>

#include "internal/drup.h"
#include "internal/transaction.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"

#define CMD_LOAD_FIRST_SECRET 0x5A
#define CMD_READ_AUTH_PAGE    0xA5
#define CMD_READ_SCRATCHPAD   0xAA
#define CMD_WRITE_SCRATCHPAD  0x0F
#define MEM_REGION_SECRET     0x80

// --- the time it takes the device to calculate the MAC (1.5 ms)
static const struct timespec T_CSHA = { .tv_sec = 0, .tv_nsec =  1.5e6, };
// --- the time it takes the device to authenticate or load the secret (10 ms)
static const struct timespec T_PROG = { .tv_sec = 0, .tv_nsec = 10e6, };

bool ds1961_generate_secret (gensecret_transaction_t transaction, uint8_t *secret);
bool ds1961_auth_attempt (auth_transaction_t transaction, bool *authenticated);

#pragma GCC diagnostic pop

#endif
