#ifndef _INTERNAL_CONTROLLER_H
#define _INTERNAL_CONTROLLER_H

#include "../drup.h"

struct controller_spec {
  const char *name;
  uint8_t num_buses;
};

struct controller_config {
  const char *i2c_dev;
  uint8_t i2c_address;
};

struct drup_controller_impl {
  controller_type_t type;
  const struct controller_spec *spec;
  const struct controller_config *config;

  // --- set during mk
  int fd;
  struct bus **buses;
  const struct bus *selected_bus;
};

struct ow_config *controller_get_selected_bus_ow_config (drup_controller_t_const t);

bool controller_device_reset (drup_controller_t_const ctrl);
bool controller_init (drup_controller_t_const t, bool mock);
bool controller_prepare_device_command (drup_controller_t_const ctrl, const uint8_t *device_id, uint8_t command);
bool controller_read_bytes (drup_controller_t_const, uint8_t *buf, uint8_t cnt);
bool controller_read_byte (drup_controller_t_const ctrl, uint8_t *b);
bool controller_write_bytes (drup_controller_t_const ctrl, const uint8_t *buf, uint8_t cnt);
bool controller_write_byte (drup_controller_t_const ctrl, uint8_t b);
/* For when an external module (e.g. drup.c) wants the controller to reset.
 * It's a separate function so that we are sure to disable SPU.
 */
bool controller_w1_reset_external (drup_controller_t_const ctrl);

bool controller_search_init (drup_controller_t ctrl, const struct bus *bus, const struct ow_config *cfg, presence_t *presence);
bool controller_search (drup_controller_t_const ctrl, drup_authenticator_t_const auth, uint8_t max_results, uint8_t device_ids[][8], uint8_t *num_results);
#endif
