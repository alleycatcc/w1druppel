#ifndef _INTERNAL_BUS_H
#define _INTERNAL_BUS_H

#include <stdint.h>

#include "drup.h"
#include "internal/drup.h"

/* The configuration as actually set during a configure command. Strong
 * pull-up is not present here: we don't allow the user to set it.
 */

struct ow_config {
  // --- may be different from the value requested by the user due to
  // `disable_overdrive_during_init`
  bool overdrive;
  bool active_pullup;
};

struct bus {
  drup_controller_t controller;
  uint8_t number;
  // --- set during search_init and then used by search/auth/generate-secret
  // calls.
  struct ow_config *ow_config;
  network_type_t network_type;
};

struct bus *bus_new (drup_controller_t ctrl, uint8_t bus_num);
void bus_destroy (struct bus *bus);

#endif
