#ifndef _INTERNAL_DS2482_H
#define _INTERNAL_DS2482_H

#include "internal/drup.h"

// --- global reset: resets 1WS, SPU, APU.
#define CMD_DEVICE_RESET             0xF0
#define CMD_SET_READ_PTR             0xE1
#define CMD_CHANNEL_SELECT           0xC3

// --- CMD_W1_RESET, CMD_W1_READ_BYTE, and CMD_W1_WRITE_BYTE use current
// values of 1WS, SPU, APU.
// --- SPU must be off before issuing CMD_W1_RESET.
#define CMD_W1_RESET                 0xB4
#define CMD_W1_TRIPLET               0x78
#define CMD_W1_READ_BYTE             0x96
#define CMD_W1_WRITE_BYTE            0xA5
#define CMD_W1_SINGLE_BIT            0x87
#define CMD_WRITE_CONFIG             0xD2

#define STATUS_BUSY                  0x01
#define STATUS_PRESENCE_PULSE_DETECT 0x02
#define STATUS_SHORT_DETECTED        0x04
#define STATUS_SINGLE_BIT_READ       0x20

#define SELECT_CODE_STATUS           0xF0
#define SELECT_CODE_DATA             0xE1
#define SELECT_CODE_CHANNEL          0xD2
#define SELECT_CODE_CONFIG           0xC3

// --- @todo actually these are general one wire commands, not controller-specific.
// --- @todo these are specified in device data sheets -- are they really
// the same for all devices?
#define W1_READ_ROM                  0x33
#define W1_MATCH_ROM                 0x55
#define W1_MATCH_ROM_OVERDRIVE       0x69
#define W1_SEARCH_ALARM              0xEC
#define W1_SEARCH_ALL                0xF0
#define W1_SKIP_ROM                  0xCC
#define W1_SKIP_ROM_OVERDRIVE        0x3C

#define CFG_REG_APU                  0x01
#define CFG_REG_SPU                  0x04
#define CFG_REG_1WS                  0x08

// --- these are what we call buses, but are called channels in the
// datasheet.
#define CHANS_SELECT (uint8_t []) { 0xF0, 0xE1, 0xD2, 0xC3, 0xB4, 0xA5, 0x96, 0x87, }
#define CHANS_SELECT_ACK (uint8_t []) { 0xB8, 0xB1, 0xAA, 0xA3, 0x9C, 0x95, 0x8E, 0x87, }

bool ds2482_device_reset (drup_controller_t_const ctrl);
bool ds2482_prepare_device_command (drup_controller_t_const ctrl, const uint8_t *device_id, uint8_t command);
bool ds2482_read_bit (drup_controller_t_const ctrl, bool *ret);
bool ds2482_read_byte (drup_controller_t_const ctrl, uint8_t *read);
bool ds2482_read_bytes (drup_controller_t_const ctrl, uint8_t *read, uint8_t count);
bool ds2482_w1_reset (drup_controller_t_const ctrl, presence_t *presence);
bool ds2482_w1_reset_external (drup_controller_t_const ctrl);
bool ds2482_write_bit (drup_controller_t_const ctrl, bool bit);
bool ds2482_write_byte (drup_controller_t_const ctrl, uint8_t byte);
bool ds2482_write_bytes (drup_controller_t_const ctrl, const uint8_t *bytes, uint8_t count);

bool ds2482_search_init (drup_controller_t ctrl, const struct bus *bus, const struct ow_config *cfg, presence_t *presence);
bool ds2482_search_start (drup_controller_t_const ctrl, drup_authenticator_t_const auth, uint8_t max_results, uint8_t device_ids[][8], uint8_t *num_results);

#endif
