#ifndef _INTERNAL_UTIL_H
#define _INTERNAL_UTIL_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"

#include <math.h>
#include <stdint.h>

#include <alleycat.h>

#define err_str_size 200
static char drup_util_err_str[err_str_size];

#define try_with(f, args, onfail) do { \
  int __rc = snprintf (drup_util_err_str, err_str_size, "%s (): failed at %s ()", __func__, #f); \
  if (__rc >= err_str_size) { \
    drup_util_err_str[err_str_size - 1] = '.'; \
    drup_util_err_str[err_str_size - 2] = '.'; \
    drup_util_err_str[err_str_size - 3] = '.'; \
  } \
  if (!f args) onfail (drup_util_err_str); \
} while (0)

/* The main form we use shows errors with `perr` and returns `false`.
 *
 * @todo perr is not always that useful -- need to sort out the calls where
 * it is and isn't.
 */
#define try(args...) do {\
  try_with (args, ac_warn_perr_rf); \
} while (0)

#define try_with_info(f, args, onfail, infofmt, infoargs...) do { \
  int __rc = snprintf (drup_util_err_str, err_str_size, "%s (): failed at %s () " infofmt, __func__, #f, ## infoargs); \
  if (__rc >= err_str_size) { \
    drup_util_err_str[err_str_size - 1] = '.'; \
    drup_util_err_str[err_str_size - 2] = '.'; \
    drup_util_err_str[err_str_size - 3] = '.'; \
  } \
  if (!f args) onfail (drup_util_err_str); \
} while (0)

#define try_info(f, args, info...) do { \
  try_with_info(f, args, ac_warn_perr_rf, info); \
} while (0)

#define retry(n, delay_ms, f, args) do { \
  for (uint8_t i$ = 0; i$ < n; i$++) { \
    if (f args) break; \
    ac_warn ("pass %d/%d failed, retrying", i$ + 1, n); \
    if (i$ == n - 1) return false; \
    if (!sleep_ms (delay_ms)) \
      ac_warn ("Sleep unsuccessful (%d ms)", delay_ms); \
  } \
} while (0)

void uint64_to_hex (uint64_t value, unsigned char *ret);
void uint8_to_hex (unsigned char *ret, uint8_t *value);

bool rand_bytes (uint8_t *b, int n);
bool rand_byte (uint8_t *b);
bool rand_bool (bool *b);

int max_int (uint8_t num_args, ...);

bool sleep_ms (unsigned int ms);

#pragma GCC diagnostic pop

#endif
