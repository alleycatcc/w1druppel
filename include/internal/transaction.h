#ifndef _INTERNAL_TRANSACTION_H
#define _INTERNAL_TRANSACTION_H

#include "internal/controller.h"

/* Base structure representing an authenticate or generate secret transaction.
 */
typedef struct transaction_impl {
  drup_controller_t_const ctrl;
  const uint8_t *device_id;
} *transaction_t;

typedef struct {
  struct transaction_impl base;
  const uint8_t *secret;
} *auth_transaction_t;

typedef struct {
  struct transaction_impl base;
} *gensecret_transaction_t;

gensecret_transaction_t gensecret_transaction_mk (drup_controller_t_const ctrl, const uint8_t *device_id);
auth_transaction_t auth_transaction_mk (drup_controller_t_const ctrl, const uint8_t *device_id, const uint8_t *secret);
void auth_transaction_destroy (auth_transaction_t t);
void gensecret_transaction_destroy (gensecret_transaction_t t);

bool transaction_prepare_device_command (transaction_t transaction, uint8_t command);
#endif
