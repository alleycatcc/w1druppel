#ifndef _INTERNAL_AUTHENTICATOR_H
#define _INTERNAL_AUTHENTICATOR_H

#include "../drup.h"
#include "transaction.h"

struct authenticator_spec {
  const char *name;
  uint64_t device_family;
  bool overdrive_default;
  // --- overdrive only works if devices are set to overdrive first, which
  // we haven't implemented for 1961, whereas 1964 always and only uses
  // overdrive. Setting `overdrive` to true for the DS1961 *might* work, but
  // for now it has to be disabled during init no matter what.
  bool disable_overdrive_during_init;
  uint8_t secret_length;
};

struct drup_authenticator_impl {
  authenticator_type_t type;
  const struct authenticator_spec *spec;
};

bool authenticator_attempt (drup_authenticator_t_const auth, auth_transaction_t auth_transaction, bool *authenticated);
bool authenticator_generate_secret (drup_authenticator_t_const auth, gensecret_transaction_t transaction, uint8_t *secret);

transaction_t transaction_mk (drup_controller_t_const ctrl, const uint8_t *device_id, const uint8_t *secret);
void transaction_destroy (transaction_t t);

uint8_t auth_max_secret_length ();

#endif
