#ifndef _INTERNAL_BENCHMARK_H
#define _INTERNAL_BENCHMARK_H

void benchmark_start (const char * const tag, ...);
void benchmark (const char * const tag, ...);

#endif

