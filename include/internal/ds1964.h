#ifndef _INTERNAL_DS1964_H
#define _INTERNAL_DS1964_H

#include "internal/drup.h"

#define COMMAND_COMPUTE_AND_READ_MAC 0xa5
#define COMMAND_LOAD_AND_LOCK 0x33
#define COMMAND_READ_STATUS 0xaa
#define COMMAND_SCRATCHPAD 0x0f
#define COMMAND_READ_MEMORY 0xf0
#define COMMAND_WRITE_MEMORY 0x55
#define RELEASE_BYTE 0xaa

bool ds1964_generate_secret (gensecret_transaction_t transaction, uint8_t *secret);
bool ds1964_auth_attempt (auth_transaction_t transaction, bool *authenticated);

#endif
