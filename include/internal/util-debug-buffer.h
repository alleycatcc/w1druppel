// --- to avoid a (spurious) compiler warning about divide by zero
static inline unsigned long long _ac_divide (unsigned long long x, unsigned long long y) {
  return x / y;
}

#ifdef AC_DEBUG

# define debug_buffer(buf, EL_TYPE, n, row_size, header) do { \
  printf ("%s\n", header); \
  bool $$do_rows; \
  size_t $$m; \
  size_t $$s; \
  size_t $$nn = 0; \
  if (row_size == 0) { \
    $$m = 1; \
    $$s = n; \
    $$do_rows = false; \
  } \
  else { \
    /* discard remainder */ \
    $$m = _ac_divide (n, row_size); \
    if (row_size * $$m != n) $$m += 1; \
    $$s = row_size; \
    $$do_rows = true; \
  } \
  for (size_t $$r = 0; $$r < $$m; $$r++) { \
    if ($$do_rows) printf ("%lu | ", $$r); \
    for (size_t $$i = 0; $$i < $$s; $$i++) { \
      if ($$i) printf (" "); \
      printf ("0x%02x", ((EL_TYPE *)buf)[$$r*$$s + $$i]); \
      $$nn += 1; \
      if ($$nn == n) break; \
    } \
    printf ("\n"); \
  } \
  printf ("\n"); \
} while (0)

#define debug_buf(buf, EL_TYPE, n, row_size, str, ...) do { \
  ac_ (); \
  ac_spr (str, ##__VA_ARGS__); \
  ac_spr ("%s\n%s", ac_s, #EL_TYPE); \
  debug_buffer (buf, EL_TYPE, n, row_size, ac_t); \
} while (0)

#else

# define debug_buffer(...) {}
# define debug_buf(...) {}

#endif
