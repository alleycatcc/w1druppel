#ifndef _INTERNAL_I2C_H
#define _INTERNAL_I2C_H

#include <stdbool.h>
#include <stdint.h>

#include "drup.h"
#include "internal/drup.h"

bool i2c_read_byte (drup_controller_t_const ctrl, uint8_t *ret);
bool i2c_cmd (drup_controller_t_const ctrl, uint8_t cmd);
bool i2c_cmd_with_block (drup_controller_t_const ctrl, uint8_t cmd, uint8_t length, const uint8_t *data);
bool i2c_cmd_with_data (drup_controller_t_const ctrl, uint8_t cmd, uint8_t data);

#endif
