/* Internal types which don't have their own .h files go here.
 */

#ifndef _INTERNAL_DRUP_H
#define _INTERNAL_DRUP_H

#include <stdbool.h>
#include <stdint.h>

#include "../drup.h"
#include "bus.h"

struct config {
  bool verbose;
};

struct drup_impl {
  struct config *config;
  drup_controller_t *controllers;
  uint8_t num_controllers;
  drup_authenticator_t *authenticators;
  uint8_t num_authenticators;
};

#endif
