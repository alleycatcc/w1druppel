#ifndef _DRUP_H
#define _DRUP_H

#include <stdbool.h>
#include <stdint.h>

// @todo add drup_
typedef enum { DS2477, DS2482_100, DS2482_800, } controller_type_t;
typedef enum { DS1961, DS1964, } authenticator_type_t;
typedef enum { PRESENCE_NONE, PRESENCE_READY, PRESENCE_WITH_SHORT, } presence_t;

/* Auto means decide based on whether it's a single- or multi-drop bus.
 * APU_AUTO_NEW follows the recommendation given in newer datasheets (APU should
 * generally be on, regardless of the bus) (see DS2477 datasheet).
 * So APU_AUTO_NEW is currently equivalent to APU_FORCE_ON.
 * APU_AUTO_OLD follows the older recommendation: APU should be on unless
 * it's a single drop bus (see DS2482 datasheet).
 */
typedef enum { APU_FORCE_OFF, APU_FORCE_ON, APU_AUTO_NEW, APU_AUTO_OLD, } active_pullup_type_t;
typedef enum { MULTI_DROP, SINGLE_DROP, } network_type_t;

/* Auto means decide based on the recommended value for each authenticator.
 * Mostly on means it may still be disabled during init according to the
 * value of `disable_overdrive_during_init`.
 */
typedef enum { OVERDRIVE_AUTO, OVERDRIVE_FORCE_OFF, OVERDRIVE_MOSTLY_ON, } overdrive_type_t;

#define validate_active_pullup_type(t) ( \
  t == APU_FORCE_OFF || \
  t == APU_FORCE_ON || \
  t == APU_AUTO_NEW || \
  t == APU_AUTO_OLD \
)

#define validate_network_type(t) ( \
  t == MULTI_DROP || \
  t == SINGLE_DROP \
)

#define validate_overdrive_type(t) ( \
  t == OVERDRIVE_AUTO || \
  t == OVERDRIVE_FORCE_OFF || \
  t == OVERDRIVE_MOSTLY_ON \
)

typedef const struct drup_authenticator_impl *drup_authenticator_t_const;
typedef const struct drup_controller_impl *drup_controller_t_const;
typedef const struct drup_impl *drup_t_const;

typedef struct drup_authenticator_impl *drup_authenticator_t;
typedef struct drup_controller_impl *drup_controller_t;
typedef struct drup_impl *drup_t;

/* The configuration as requested by the user for a search/auth/secret
 * transaction. Doesn't get stored in the bus struct.
 */

struct drup_bus_user_config {
  // --- can be overridden depending on the value of
  // `disable_overdrive_during_init`
  overdrive_type_t overdrive;
  active_pullup_type_t active_pullup_type;
  network_type_t network_type;
};

struct drup_authenticator_view {
  const char *name;
  authenticator_type_t type;
  uint8_t device_family;
  bool overdrive_default;
  bool disable_overdrive_during_init;
  uint8_t secret_length;
};

struct drup_controller_view {
  const char *name;
  controller_type_t type;
  const char *i2c_dev;
  uint8_t i2c_address;
  uint8_t num_buses;
};

struct drup_view {
  uint8_t num_controllers;
  uint8_t num_authenticators;
};

drup_authenticator_t drup_authenticator_mk (authenticator_type_t type);
void drup_authenticator_destroy (drup_authenticator_t t);
struct drup_authenticator_view drup_authenticator_view (drup_authenticator_t_const t);

drup_controller_t drup_controller_mk (controller_type_t type, const char *i2c_dev, uint8_t i2c_address, bool mock);
void drup_controller_destroy (drup_controller_t t);
struct drup_controller_view drup_controller_view (drup_controller_t_const t);
uint8_t drup_controller_num_buses (drup_controller_t_const t);

drup_t drup_mk (drup_controller_t *ctrls, uint8_t numc, drup_authenticator_t *auths, uint8_t numa, bool verbose, bool mock);
void drup_destroy (drup_t drup);
void drup_authenticator_views (drup_t_const drup, struct drup_authenticator_view *ret);
bool drup_authenticator_view_at (drup_t_const drup, uint8_t auth_num, struct drup_authenticator_view *view);
struct drup_view drup_view (drup_t_const drup);

bool drup_authenticate (drup_t_const drup, uint8_t ctrl_num, uint8_t auth_num, uint8_t *device_id, const uint8_t *secret, bool *authenticated);
bool drup_generate_secret (drup_t_const drup, uint8_t ctrl_num, uint8_t auth_num, uint8_t *device_id, uint8_t *secret);
bool drup_search (drup_t_const drup, uint8_t ctrl_num, uint8_t auth_num, uint8_t max_results, uint8_t device_id[][8], unsigned char device_id_str[][17], uint8_t *num_results);
bool drup_search_init (drup_t_const drup, uint8_t ctrl_num, uint8_t bus_num, uint8_t auth_num, struct drup_bus_user_config *ow_cfg, presence_t *presence);
struct drup_cfg *drup_config (bool overdrive, bool disable_overdrive_during_init, bool active_pullup);

uint8_t drup_get_secret_length_for_auth_num (drup_t_const drup, uint8_t auth_num);
bool drup_get_num_buses (drup_t_const drup, uint8_t ctrl_num, uint8_t *ret);

#endif
