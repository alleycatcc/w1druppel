ar = ar rs
shared = -shared
libs = -lalleycat -lm -lcrypto
libs-test = -lbsd

cc = $(CC) -Wall -Wextra -fPIC $(ccextra)

.PHONY: all prepare alleycat-c test clean mrproper shared base

cflags += \
        -Iinclude \
        -Ilib/alleycat-c/include \
        -Ilib/crc/include \
        -Ilib/i2c \
        -Ilib/sha33/include \
        -Ilib/sha256/include \

target-shared = .lib/libw1druppel.so

ifdef AC_DEBUG
	cflags += -DAC_DEBUG=$(AC_DEBUG)
endif
ifdef DO_BENCHMARK
	cflags += -DDO_BENCHMARK=$(DO_BENCHMARK)
endif
ifdef MOCK
	cflags += -DMOCK=$(MOCK)
endif

ldflags = -Llib/alleycat-c/build/static

headers = $(wildcard include/*.h) \
          $(wildcard include/**/*.h)

src = $(wildcard src/*.c) \
	  lib/crc/crc8-dow.c \
	  lib/crc/crc16.c \
      lib/sha33/src/sha33.c \
      lib/sha256/sha256.c \

objects = .obj/authenticator.o \
          .obj/benchmark.o \
          .obj/bus.o \
          .obj/controller.o \
          .obj/crc.o \
          .obj/drup.o \
          .obj/ds1961.o \
          .obj/ds1964.o \
          .obj/ds2477.o \
          .obj/ds2482.o \
          .obj/i2c.o \
          .obj/transaction.o \
          .obj/util.o \
          .obj/drup-util.o \
		  .obj/lib/sha33.o \
		  .obj/lib/sha256.o \
		  .obj/lib/crc8-dow.o \
		  .obj/lib/crc16.o

all: prepare shared
	@echo Successfully compiled shared library. Other targets: test

.obj/%.o: src/%.c $(headers)
	$(cc) -c $(cflags) $< -o $@

.obj/lib/sha33.o: lib/sha33/src/sha33.c
	$(cc) -c $(cflags) $< -o $@

.obj/lib/sha256.o: lib/sha256/sha256.c
	$(cc) -c $(cflags) $< -o $@

.obj/lib/crc8-dow.o: lib/crc/crc8-dow.c
	$(cc) -c $(cflags) $< -o $@

.obj/lib/crc16.o: lib/crc/crc16.c
	$(cc) -c $(cflags) $< -o $@

base: $(objects) $(headers) $(src)

$(target-shared):
	$(cc) $(cflags) .obj/*.o .obj/lib/*.o $(ldflags) -shared -o $(target-shared)

shared: base .lib/libw1druppel.so

test/drup: .obj test/drup.c
	$(cc) $(cflags) test/drup.c .obj/*.o .obj/lib/*.o -o test/drup $(ldflags) $(libs) $(libs-test)

prepare: alleycat-c
	mkdir -p .obj .obj/lib .lib

alleycat-c:
	make -C lib/alleycat-c

test: prepare shared test/drup

clean:
	rm -rf test/drup .obj .lib

mrproper: clean
