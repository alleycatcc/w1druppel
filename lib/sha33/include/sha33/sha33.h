#ifndef SHA33_H
#define SHA33_H

#include <stdint.h>

void sha33_compute_sha_vm_32 (const uint32_t MT[], uint32_t hash[]);
void sha33_compute_sha_vm_8 (const uint8_t MT[], uint32_t hash[]);
void sha33_hash_to_mac (const uint32_t hash[], uint8_t MAC[]);

#endif
