/* DOW (Dallas One-Wire) specific CRC8 routine.
 * Polynomial is x^8 + x^5 + x^4 + 1, the seed is 0, and the CRC is received
 * non-inverted.
 * The lookup table is from Maxim's documentation.
 */

#ifndef _CRC8_DOW_H
#define _CRC8_DOW_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

uint8_t crc8 (uint8_t *data, size_t n);
bool crc8_check (uint8_t *data, size_t n, uint8_t crc_check);
/* Expects the CRC as the last byte of the input. Calculating the CRC of
 * this entire input results in 0 if the last byte is correct.
 */
bool crc8_ok (uint8_t *data, size_t n);

#endif
