/* Standard CRC-16. Sometimes called CRC16_ARC, or ANSI.
 *
 * Polynomial: x^16 + x^15 + x^2 + 1 i.e. 0x8005.
 *
 *  - Width         = 16
 *  - Poly          = 0x8005
 *  - XorIn         = 0x0000
 *  - ReflectIn     = True
 *  - XorOut        = 0x0000
 *  - ReflectOut    = True
 *
 * C code can be generated with for example pycrc using the above
 * parameters.
 *
 * CRC bytes are expected to be inverted.
 *
 * The order is always tricky: the functions expect the bytes in the order
 * they are received from the device. As a uint16_t it's (byte0 | (byte1 << 8)
 */

#ifndef _CRC16_H
#define _CRC16_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

uint16_t crc16 (const uint8_t *data, size_t n);
bool crc16_check (const uint8_t *data, size_t n, uint8_t crc_check0, uint8_t crc_check1);
/* Expects the CRC as the last two bytes of the input. Calculating the CRC
 * of this entire input results in 0 if the last two byte are correct.
 */
bool crc16_ok (const uint8_t *data, size_t n);

#endif

