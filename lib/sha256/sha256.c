/*
 * Copyright 2023 alleycatcc GPL-3 or newer
 *
 * Based on ds28el15_sha256.c, copyright (C) 2013 maximintergrated
 *
 * Original license message:
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License version 2 as
 *     published by the Free Software Foundation.
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "include/sha256.h"

#define ushort unsigned short
#define ulong unsigned long

#define maj(x, y, z) ((x & y) ^ (x & z) ^ (y & z))
#define ch(x, y, z) ((x & y) ^ ((~x) & z))
#define shr_32(x, y) ((x & 0xFFFFFFFFL) >> y)
#define rotr_32_x(x) (x & 0xFFFFFFFFL)
#define rotr_32(x, y) (((rotr_32_x (x) >> y) | (rotr_32_x (x) << (32 - y))) & 0xFFFFFFFFL)
#define bigsigma256_0(x) (rotr_32 (x, 2) ^ rotr_32 (x, 13) ^ rotr_32 (x, 22))
#define bigsigma256_1(x) (rotr_32 (x, 6) ^ rotr_32 (x, 11) ^ rotr_32 (x, 25))
#define littlesigma256_0(x) (rotr_32 (x, 7) ^ rotr_32 (x, 18) ^ shr_32 (x, 3))
#define littlesigma256_1(x) (rotr_32 (x, 17) ^ rotr_32 (x, 19) ^ shr_32 (x, 10))

static ulong initial[] = {
  0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
  0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19,
};

static ulong constants[] = {
  0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
  0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
  0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
  0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
  0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
  0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
  0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
  0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2,
  0xca273ece, 0xd186b8c7, 0xeada7dd6, 0xf57d4f7f, 0x06f067aa, 0x0a637dc5, 0x113f9804, 0x1b710b35,
  0x28db77f5, 0x32caab7b, 0x3c9ebe0a, 0x431d67c4, 0x4cc5d4be, 0x597f299c, 0x5fcb6fab, 0x6c44198c,
};

static void copy_words_to_bytes (uint8_t *out, const ulong *in, ushort numwords) {
  for (ushort i = 0; i < numwords; i++) {
    ulong word = *in++;
    *out++ = (uint8_t) (word >> 24);
    *out++ = (uint8_t) (word >> 16);
    *out++ = (uint8_t) (word >> 8);
    *out++ = (uint8_t) word;
  }
}

static void write_result (ulong *in, bool reverse, uint8_t* ret) {
  copy_words_to_bytes (ret, in, 8);
  if (!reverse) return;
  for (ushort i = 0; i < 16; i++) {
    uint8_t x = ret[i];
    ret[i] = ret[31-i];
    ret[31-i] = x;
  }
}

static void prepare_schedule (const uint8_t * const msg, ulong * const W32) {
  uint8_t *src = (uint8_t *) msg;
  for (ushort i = 0; i < 16; i++) {
     ulong w = 0;
     for (ushort j = 0; j < 4; j++) {
        w = w << 8;
        w = w | (*src++ & 0xff);
     }
     W32[i] = w;
  }
}

static ulong getw (ulong * const W32, int index) {
  if (index < 16) return W32[index];

  ulong newW =
    littlesigma256_1 (W32[(index-2) & 0x0f]) +
    W32[(index-7) & 0x0f] +
    littlesigma256_0 (W32[(index-15) & 0x0f]) +
    W32[(index-16) & 0x0f];
  W32[index & 0x0f] = newW & 0xFFFFFFFFL;  // just in case...

  return newW;
}

static void hash_block (const uint8_t * const msg, ulong * const H32, bool no_add_const) {
  ulong Wt, Kt;
  ulong a32, b32, c32, d32, e32, f32, g32, h32;

  ulong W32[16];

  prepare_schedule (msg, W32);
  a32 = H32[0];
  b32 = H32[1];
  c32 = H32[2];
  d32 = H32[3];
  e32 = H32[4];
  f32 = H32[5];
  g32 = H32[6];
  h32 = H32[7];

  for (ushort i = 0; i < 64; i++) {
     Wt = getw (W32, i);
     Kt = constants[i];

     ulong nodeT1 = h32 + bigsigma256_1 (e32) + ch (e32, f32, g32) + Kt + Wt; // & 0xFFFFFFFFL;
     ulong nodeT2 = bigsigma256_0 (a32) + maj (a32, b32, c32); // & 0xFFFFFFFFL;
     h32 = g32;
     g32 = f32;
     f32 = e32;
     e32 = d32 + nodeT1;
     d32 = c32;
     c32 = b32;
     b32 = a32;
     a32 = nodeT1 + nodeT2;
  }

  if (no_add_const) {
     H32[0] = a32;
     H32[1] = b32;
     H32[2] = c32;
     H32[3] = d32;
     H32[4] = e32;
     H32[5] = f32;
     H32[6] = g32;
     H32[7] = h32;
  }
  else {
     H32[0] += a32;
     H32[1] += b32;
     H32[2] += c32;
     H32[3] += d32;
     H32[4] += e32;
     H32[5] += f32;
     H32[6] += g32;
     H32[7] += h32;
  }
}

// --- we only accept `length` > 64, or else we set the result to all zeros.
// --- assumes word size = 32, bytes per block = 64
static void compute_sha256 (const uint8_t* const msg, ushort length, ushort skip_const_on_last_block, bool reverse_out, uint8_t *out) {
  if (length <= 64) {
    memset (out, 0, 32);
    return;
  }

  ulong H32[8];
  memcpy (H32, initial, 8 * sizeof (ulong));

  // 1 byte for the '80' that follows the message, 8 or 16 bytes of length
  ushort nonpaddedlength = length + 1 + 8;
  ushort numblocks = nonpaddedlength / 64;
  if ((nonpaddedlength % 64) != 0)
    numblocks++;

  for (ushort i = 0; i < numblocks; i++) {
    bool lastblock = i == numblocks - 1;
    hash_block (msg + i*64, H32, (ushort) (lastblock && skip_const_on_last_block));
  }
  write_result (H32, reverse_out, out);
}

void compute_mac_sha256_maxim (uint8_t *input, uint8_t *ret) {
  compute_sha256 (input, 119, 1, 1, ret);
}
