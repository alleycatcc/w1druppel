/*
 * 1-Wire SHA256 software implementation for the ds23el15 chip
 *
 * Copyright (C) 2013 maximintergrated
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#ifndef _W1_DS28EL15_SHA256_H
#define _W1_DS28EL15_SHA256_H

#include <stdint.h>

void compute_mac_sha256_maxim(uint8_t *message, uint8_t *digest);

#endif
