import {
  pipe, compose, composeRight,
  addIndex, repeatF, eq,
  id, ifOk, ifPredicate, recurry, always
} from 'stick-js/es'

import { length, } from 'alleycat-js/es/general'

export const N = always (null)
export const flatMap = recurry (2) (
  (f) => (xs) => xs.flatMap (f),
)
export const flatMapX = addIndex (flatMap)
const flatten = flatMap (id)
export const flatRepeatF = recurry (2) (
  (f) => (o) => repeatF (f) (o) | flatten,
)
export const isEmptyList = length >> eq (0)
export const ifEmptyList = isEmptyList | ifPredicate

export const discardStackTrace = (e) => e.message | ifOk (id, () => e)

export const randInt = (x) => Math.floor (Math.random () * x)
export const randBool = () => Boolean (randInt (2))

export const base64decodeAsU8Array = (x) => new Uint8Array (Buffer.from (x, 'base64'))
export const base64encodeFromU8Array = (x) => Buffer.from (x).toString ('base64')
