import {
  pipe, compose, composeRight,
  filter, invoke, T, F,
  noop, each, sprintfN, id, prop, map,
  lets, not, die, tryCatch, ifNil,
} from 'stick-js/es'

import { createRequire, } from 'module'
const require = createRequire (import.meta.url)
const druppelMod = require ('../../build/Release/w1druppel')

import { searchInit, search, authenticate, generateSecret, } from './drup.mjs'
import { toMock, } from './types.mjs'

import {
  discardStackTrace, flatMap, flatMapX, flatRepeatF,
  isEmptyList, N,
} from './util.mjs'

/*
 * If the user's error handler throws an exception, we catch it, but we
 * don't do anything special after that. In the case of the 'should'
 * functions we return the obvious conservative value (false, null) etc. and
 * for search we return an empty list.
 *
 * If the user requires something special to happen when their own handler
 * throws an exception, then they should use put a try/catch around it.
 */

const tryHandler = invoke (() => {
  const error = (tag, e) => console.error (
    [tag, e | discardStackTrace] | sprintfN (
      'tryHandler (): caught error during event handler %s (): %s',
    ),
  )
  // --- `onOk/Error` means, if the handler itself throws an error.
  return (tag, onOk, onError, handler) => tryCatch (
    onOk,
    (e) => (error (tag, e), onError (e)),
    handler,
  )
})

/* Interact means generate secret or authenticate.
 * `shouldFunctionDone` returns a (possibly empty) list of device ids (for
 * `shouldGenerateSecret` or a (possibly empty) list of tuples (deviceId,
 * secret) (for `shouldAuthenticate`)
 */
const interact = (tag1, tag2, shouldFunction, shouldFunctionDone, doFunction, onComplete) => (results) => {
  results
  | flatMap ((deviceId) => {
    // --- `shouldOrSecret` is 'should' in the case of `shouldAuthenticate`
    // and 'secret' in the case of `shouldGenerateSecret`
    const [err, shouldOrSecret] = tryHandler (
      tag1,
      (shouldOrSecret) => [null, shouldOrSecret],
      (e) => [e, null],
      () => shouldFunction (deviceId),
    )
    return err | ifNil (
      // --- should is the secret in the case of authenticate
      () => shouldFunctionDone (shouldOrSecret, deviceId),
      (e) => {
        console.error ('Error during handler', tag1 + ':', discardStackTrace (e))
        return []
      },
    )
  })
  | each (([deviceId, ...rest]) => {
    // --- result is secret / authenticateSucceeds
    const [err, result] = doFunction (deviceId, ...rest)
    return err | ifNil (
      () => tryHandler (tag2, noop, noop, () => onComplete (deviceId, result)),
      (e) => {
        console.error ('Error during handler', tag2 + ':', discardStackTrace (e))
        return []
      },
    )
  })
}

// --- returns an empty list in all cases (see `once`))

const inner = (drup, mock) => (ctrlNum, ctrlTag, busNum, authNum, owConfig) => ({
  onError=noop, onShort=noop, onPresence=T, onResult=T, shouldGenerateSecret=F,
  onGenerateSecret=noop, shouldAuthenticate=N, onAuthenticate=noop,
}) => {
  const info = { ctrlNum, ctrlTag, busNum, authNum, }

  const [errorInit, presence, ready] = searchInit (drup, mock) (ctrlNum, busNum, authNum, owConfig)

  if (errorInit) return tryHandler (
    'onError', () => [], () => [],
    () => onError (errorInit, info),
  )
  if (!presence) return []
  if (not (ready)) return tryHandler (
    'onShort', () => [], () => [],
    () => onShort (info),
  )

  // --- @future collapse shorts perhaps, meaning if you see a short on one
  // authenticator then maybe it's not useful to generate another
  // notification for subsequent ones

  if (!tryHandler (
    'onPresence', id, F,
    () => onPresence (info),
  )) return []

  const [errorSearch, results, empty] = search (drup, mock) (ctrlNum, authNum)
  if (errorSearch) return tryHandler (
    'onError', () => [], () => [],
    () => onError (errorSearch, info),
  )
  if (empty) return []

  // --- `onResult` returning false means drop this result from further
  // processing (either an error occurred during the handler, or the user's
  // `onResult` callback indicated that they don't want to continue)
  const resultsRemaining = results | filter (
    (deviceId) => tryHandler (
      'onResult', id, F,
      () => onResult (deviceId, info),
    ),
  )

  if (resultsRemaining | isEmptyList) return []

  resultsRemaining | interact (
    'shouldGenerateSecret', 'onGenerateSecret',
    (deviceId) => shouldGenerateSecret (deviceId, info),
    (should, deviceId, _) => should ? [[deviceId]] : [],
    (deviceId) => generateSecret (drup, mock) (ctrlNum, authNum, deviceId),
    (deviceId, secret) => onGenerateSecret (deviceId, secret, info),
  )

  resultsRemaining | interact (
    'shouldAuthenticate', 'onAuthenticate',
    (deviceId) => shouldAuthenticate (deviceId, info),
    (secret, deviceId) => secret ? [[deviceId, secret]] : [],
    (deviceId, secret) => authenticate (drup, mock) (ctrlNum, authNum, deviceId, secret),
    (deviceId, succeeds) => onAuthenticate (deviceId, succeeds, info),
  )

  return []
}

/* Returns an empty list in all cases, success or failure. Maybe we'll
 * think of something useful to put in this array at some point, but since
 * everything is handled by handlers, that time is not now.
 *
 * @future Add a busNumPredicate function to only search certain buses
 * perhaps.
 */
const once = (drup, mock) => (ctrlTags, authenticators, owConfig) => (handlers) => {
  return ctrlTags | flatMapX (
    (ctrlTag, ctrlNum) => lets (
      // @throws
      () => drup.getNumBuses (ctrlNum),
      (numBuses) => numBuses | flatRepeatF (
        (busNum) => authenticators | flatMapX (
          (_auth, authNum) => inner (drup, mock) (ctrlNum, ctrlTag, busNum, authNum, owConfig) (handlers),
        ),
      ),
    ),
  )
}

// --- `mock`: `false`, 'random-normal', 'random-fast', 'random-fastest', ...
export const init = ({ controllers, authenticators, verbose=false, }, { mock=false, } = {}) => {
  const [ctrlTags, ctrls] = controllers | ifNil (
    () => die ('missing key “controllers”'),
    (cs) => [
      // --- mapping from ctrl num to tag
      cs | map (prop ('tag')),
      cs | map (prop ('config'))
    ],
  )

  const config = {
    verbose,
    authenticators,
    controllers: ctrls,
  }

  // --- also validates
  const drup = druppelMod.init (config, { mock: Boolean (mock), })

  return {
    once (owConfig, handlers, mockSpec=false) {
      return once (drup, toMock (mockSpec)) (ctrlTags, authenticators, owConfig) (handlers)
    },
    forever (owConfig, ms, handlers, mockSpec=false) {
      const handle = {
        _cancel: false,
        cancel () { this._cancel = true },
      }
      const loop = () => {
        this.once (owConfig, handlers, mockSpec)
        if (handle._cancel) return
        setTimeout (loop, ms)
      }
      setTimeout (loop, 0)
      return handle
    },
    cleanup () {
      drup.cleanup ()
    }
  }
}
