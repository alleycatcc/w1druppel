import {
  pipe, compose, composeRight,
  invoke, ifOk, id, mergeTo,
} from 'stick-js/es'

// --- must not depend on types
import { base64encodeFromU8Array, } from './util.mjs'

export const mockDeviceId = '0123456789abcdef'
export const mockError = 'mock-failure'
export const mockSecret = new Uint8Array ([0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7])
export const mockSecretBase64 = base64encodeFromU8Array (mockSecret)

// --- total per block is arbitrary (these are relative weights)
export const getMockChances = invoke (() => {
  const o = {
    'random-normal': () => ({
      searchInit: {
        error: 1,
        noPresence: 7000,
        presenceWithShort: 15,
        presenceReady: 15,
      },
      search: {
        error: 1,
        zeroResults: 100,
        oneResult: 500,
      },
      generateSecret: {
        error: 1,
        ok: 100,
      },
      authenticate: {
        error: 1,
        authenticate: 10,
      },
    }),
    'random-fast': () => ({
      searchInit: {
        error: 1,
        noPresence: 700,
        presenceWithShort: 15,
        presenceReady: 15,
      },
      search: {
        error: 1,
        zeroResults: 100,
        oneResult: 500,
      },
      generateSecret: {
        error: 1,
        ok: 100,
      },
      authenticate: {
        error: 1,
        authenticate: 10,
      },
    }),
    'random-fastest': () => ({
      searchInit: {
        error: 1,
        noPresence: 100,
        presenceWithShort: 15,
        presenceReady: 15,
      },
      search: {
        error: 1,
        zeroResults: 100,
        oneResult: 500,
      },
      generateSecret: {
        error: 1,
        ok: 100,
      },
      authenticate: {
        error: 1,
        authenticate: 10,
      },
    }),
    searchInit: (which) => ({
      searchInit: { [which]: 1, } | mergeTo ({
        error: 0,
        noPresence: 0,
        presenceWithShort: 0,
        presenceReady: 0,
      }),
    }),
    search: (which) => ({
      search: { [which]: 1, } | mergeTo ({
        error: 0,
        zeroResults: 0,
        oneResult: 0,
      })
    }),
    generateSecret: (which) => ({
      generateSecret: { [which]: 1, } | mergeTo ({
        error: 0,
        ok: 0,
      }),
    }),
    authenticate: (which) => ({
      authenticate: { [which]: 1, } | mergeTo ({
        error: 0,
        authenticate: 0,
      })
    }),
  }
  return (profile, ...args) => {
    if (profile === false) return null
    return o [profile] (...args) | ifOk (
      id,
      () => {
        console.error ('getMockChances (): invalid profile ' + profile + ', using default')
        return o.get (true)
      },
    )
  }
})
