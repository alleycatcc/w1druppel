import {
  pipe, compose, composeRight,
  T, F, invoke, die,
} from 'stick-js/es'

import daggy from 'daggy'

import { cata, } from 'alleycat-js/es/bilby'

import { getMockChances, mockDeviceId, mockError, mockSecret, } from './mock.mjs'
import { randBool, } from './util.mjs'

const Mock = daggy.taggedSum ('Mock', {
  MockNone: [],
  MockRandom: ['chances'],
  MockSearchInitError: ['msg'],
  MockSearchInitNoPresence: [],
  MockSearchInitPresenceWithShort: [],
  MockSearchInitPresenceReady: [],
  MockSearchError: ['msg'],
  MockSearchResults: ['deviceIds'],
  MockGenerateSecretError: ['msg'],
  MockGenerateSecretOk: ['deviceId', 'secret'],
  MockAuthenticateError: ['msg'],
  MockAuthenticateAuthenticate: ['succeeds'],
  MockTestProbe: ['deviceId', 'succeeds']
})

const {
  MockNone,
  MockRandom,
  MockSearchInitError, MockSearchInitNoPresence, MockSearchInitPresenceWithShort, MockSearchInitPresenceReady,
  MockSearchError, MockSearchResults,
  MockGenerateSecretError, MockGenerateSecretOk,
  MockAuthenticateError, MockAuthenticateAuthenticate, MockTestProbe,
} = Mock

export {
  MockNone,
  MockRandom,
  MockSearchInitError, MockSearchInitNoPresence, MockSearchInitPresenceWithShort, MockSearchInitPresenceReady,
  MockSearchError, MockSearchResults,
  MockGenerateSecretError, MockGenerateSecretOk,
  MockAuthenticateError, MockAuthenticateAuthenticate, MockTestProbe,
}

export const toMock = invoke (() => {
  const m = new Map ([
    [false, MockNone],
    ['random-normal', MockRandom (getMockChances ('random-normal'))],
    ['random-fast', MockRandom (getMockChances ('random-fast'))],
    ['random-fastest', MockRandom (getMockChances ('random-fastest'))],
    ['search-init-error', MockSearchInitError (mockError)],
    ['search-init-no-presence', MockSearchInitNoPresence],
    ['search-init-presence-with-short', MockSearchInitPresenceWithShort],
    ['search-init-presence-ready', MockSearchInitPresenceReady],
    ['search-error', MockSearchError (mockError)],
    ['search-zero-results', MockSearchResults ([])],
    ['search-one-result', MockSearchResults ([mockDeviceId])],
    ['generate-secret-error', MockGenerateSecretError (mockError)],
    ['generate-secret-ok', MockGenerateSecretOk (mockDeviceId, mockSecret)],
    ['authenticate-error', MockAuthenticateError (mockError)],
    // ------ fails and succeeds refer here to the `authenticate` step,
    // where succeeds means that the secret matches the secret in the device
    // and there are no I/O errors.
    ['authenticate-fails', MockAuthenticateAuthenticate (false)],
    ['authenticate-succeeds', MockAuthenticateAuthenticate (true)],
    /* These are the ones to use for testing kattenluik as they most closely
     * mimic a key being held in front of the probe. The flow will then
     * continue with (mock) generate secret and (mock) authenticate
     * according to the caller's own `shouldGenerateSecret` and
     * `shouldAuthenticate` logic, which is why this one is useful. It is
     * implemented by setting the probabilities for searchInit, search,
     * generateSecret, and authenticate all to 1.
     */
    ['test-probe-authenticate-fails', MockTestProbe (mockDeviceId, false)],
    ['test-probe-authenticate-succeeds', MockTestProbe (mockDeviceId, true)],
  ])
  return (mockSpec) => m.get (mockSpec) ?? die ('Invalid mock value: ' + String (mockSpec))
})

export const mockBool = cata ({
  MockNone: F,
  MockRandom: T,
  MockSearchInitError: T,
  MockSearchInitNoPresence: T,
  MockSearchInitPresenceWithShort: T,
  MockSearchInitPresenceReady: T,
  MockSearchError: T,
  MockSearchResults: T,
  MockGenerateSecretError: T,
  MockGenerateSecretOk: T,
  MockAuthenticateError: T,
  MockAuthenticateAuthenticate: T,
  MockTestProbe: T,
})

/* We provide two kinds of mocking: 'random', in which events are fired
 * randomly according to the probabilities given by `getMockChances`, and
 * 'specific', in which we can cause a specific event to fire (e.g.
 * 'generate-secret-error' or 'authenticate-deny'). They both route through
 * the same function, which is why it's called `randomOrSpecific`. In the
 * specific case we alter the probability distribution so that the case we
 * want is 1 and all the others are 0.
 *
 * Note that for the specific case, when mocking for example a generate
 * secret event, we also need to make sure we mock searchInit and search
 * with 100% probability, or else we will never reach the point we want.
 * This is done in the functions at the top of each fold function (e.g.
 * `ready`, `gotResults`, etc.).
 */

export const foldMockSearchInit = invoke (() => {
  // @todo mockDeviceId ('arg') is not used here
  const ready = (randomOrSpecific) => randomOrSpecific (getMockChances ('searchInit', 'presenceReady'), mockDeviceId)
  return (none, randomOrSpecific) => cata ({
    MockNone: () => none (),
    MockRandom: (chances) => randomOrSpecific (chances),
    MockSearchInitError: (msg) => randomOrSpecific (getMockChances ('searchInit', 'error'), msg),
    MockSearchInitNoPresence: () => randomOrSpecific (getMockChances ('searchInit', 'noPresence')),
    MockSearchInitPresenceWithShort: () => randomOrSpecific (getMockChances ('searchInit', 'presenceWithShort')),
    MockSearchInitPresenceReady: () => ready (randomOrSpecific),
    MockSearchError: (_) => ready (randomOrSpecific),
    MockSearchResults: (_deviceIds) => ready (randomOrSpecific),
    MockGenerateSecretError: (_) => ready (randomOrSpecific),
    MockGenerateSecretOk: (_) => ready (randomOrSpecific),
    MockAuthenticateError: (_) => ready (randomOrSpecific),
    MockAuthenticateAuthenticate: () => ready (randomOrSpecific),
    MockTestProbe: () => ready (randomOrSpecific),
  })
})

export const foldMockSearch = invoke (() => {
  const zeroOrOneResult = (randomOrSpecific, haveResult=true) => randomOrSpecific (
    getMockChances ('search', haveResult ? 'oneResult' : 'zeroResults'), mockDeviceId,
  )
  return (none, randomOrSpecific, other) => cata ({
    MockNone: () => none (),
    MockRandom: (chances) => randomOrSpecific (chances, mockDeviceId),
    MockSearchInitError: (_) => other (),
    MockSearchInitNoPresence: () => other (),
    MockSearchInitPresenceWithShort: () => other (),
    MockSearchInitPresenceReady: () => other (),
    MockSearchError: (msg) => randomOrSpecific (getMockChances ('search', 'error'), msg),
    MockSearchResults: (_deviceIds) => {
      const have = _deviceIds.length !== 0
      return zeroOrOneResult (randomOrSpecific, have)
    },
    MockGenerateSecretError: (_) => zeroOrOneResult (randomOrSpecific),
    MockGenerateSecretOk: (_) => zeroOrOneResult (randomOrSpecific),
    MockAuthenticateError: (_) => zeroOrOneResult (randomOrSpecific),
    MockAuthenticateAuthenticate: () => zeroOrOneResult (randomOrSpecific),
    MockTestProbe: () => zeroOrOneResult (randomOrSpecific),
  })
})

export const foldMockGenerateSecret = invoke (() => {
  const generatedSecret = (randomOrSpecific) => randomOrSpecific (
    getMockChances ('generateSecret', 'ok'), mockSecret,
  )
  return (none, randomOrSpecific, other) => cata ({
    MockNone: () => none (),
    MockRandom: (chances) => randomOrSpecific (chances, mockSecret),
    MockSearchInitError: (_) => other (),
    MockSearchInitNoPresence: () => other (),
    MockSearchInitPresenceWithShort: () => other (),
    MockSearchInitPresenceReady: () => other (),
    MockSearchError: (_) => other (),
    MockSearchResults: (_) => other (),
    MockGenerateSecretError: (_) => randomOrSpecific (getMockChances ('generateSecret', 'error'), mockError),
    MockGenerateSecretOk: (_deviceId, _secret) => generatedSecret (randomOrSpecific),
    MockAuthenticateError: (_) => generatedSecret (randomOrSpecific),
    MockAuthenticateAuthenticate: () => generatedSecret (randomOrSpecific),
    MockTestProbe: () => generatedSecret (randomOrSpecific),
  })
})

export const foldMockAuthenticate = (none, randomOrSpecific, other) => invoke (() => {
  const authenticate = (randomOrSpecific, succeeds) => randomOrSpecific (
    getMockChances ('authenticate', 'authenticate'), succeeds,
  )
  return cata ({
    MockNone: () => none (),
    MockRandom: (chances) => randomOrSpecific (chances, randBool ()),
    MockSearchInitError: (_) => other (),
    MockSearchInitNoPresence: () => other (),
    MockSearchInitPresenceWithShort: () => other (),
    MockSearchInitPresenceReady: () => other (),
    MockSearchError: (_) => other (),
    MockSearchResults: (_) => other (),
    MockGenerateSecretError: (_) => other (),
    MockGenerateSecretOk: (_) => other (),
    MockAuthenticateError: (_) => randomOrSpecific (getMockChances ('authenticate', 'error'), mockError),
    MockAuthenticateAuthenticate: (succeeds) => authenticate (randomOrSpecific, succeeds),
    MockTestProbe: (_deviceId, succeeds) => authenticate (randomOrSpecific, succeeds),
  })
})
