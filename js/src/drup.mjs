import {
  pipe, compose, composeRight,
  invoke, values,
  lets, guard, condS, otherwise, eq, guardV,
  die, tryCatch, ifNil, findIndex,
  reduce, appendM, tap,
} from 'stick-js/es'

import {
  foldMockSearchInit, foldMockSearch,
  foldMockGenerateSecret, foldMockAuthenticate,
} from './types.mjs'

import {
  discardStackTrace, ifEmptyList,
  randInt,
} from './util.mjs'

/* Takes the dictionary of chances, e.g.
 *     { case1: 10, case2: 2, case3: 5, }
 * and returns a list whose values we can compare with a random int, and the
 * last value of the list:
 *     [[10, 12, 17], 17]
 */

const makeDistribution = (o) => o | values | reduce (
  ([xs, n], y) => [xs | appendM (n + y), n + y],
  [[], 0]
)

const mockThing = (which) => (mockChances) => invoke (() => {
  const [theChances, n] = makeDistribution (mockChances [which])
  const f = (y) => theChances | findIndex ((x) => y < x)
  return (...fs) => lets (
    () => randInt (n),
    (y) => fs [f (y)] (),
  )
})

const mockSearchInit = mockThing ('searchInit')
const mockSearch = mockThing ('search')
const mockGenerateSecret = mockThing ('generateSecret')
const mockAuthenticate = mockThing ('authenticate')

export const searchInit = (drup, mock) => (ctrlNum, busNum, authNum, owConfig) => tryCatch (
  (presence) => presence | ifNil (
    () => [null, null, false],
    () => presence | condS ([
      eq ('presence-with-short') | guardV ([null, true, false]),
      eq ('presence-ready') | guardV ([null, true, true]),
      otherwise | guard (() => guardV (['Unexpected value for presence: ' + presence, null, null])),
    ]),
  ),
  (e) => ['Error on searchInit (): ' + discardStackTrace (e), null, false],
  () => mock | foldMockSearchInit (
    // --- no mock
    () => drup.searchInit (ctrlNum, busNum, authNum, owConfig),
    // --- random or specific
    (mockChances, arg) => mockSearchInit (mockChances) (
      () => die ('[mock] I/O error during searchInit:', arg),
      () => null,
      () => 'presence-with-short',
      () => 'presence-ready',
    ),
  ),
)

export const search = (drup, mock) => (ctrlNum, authNum) => tryCatch (
  ifEmptyList (
    () => [null, [], true],
    (results) => [null, results, false],
  ),
  (e) => ['Error on search (): ' + discardStackTrace (e), null, false],
  () => mock | foldMockSearch (
    // --- no mock
    () => drup.search (ctrlNum, authNum),
    // --- random or specific
    (mockChances, arg) => mockSearch (mockChances) (
      () => die ('[mock] I/O error during search:', arg),
      () => [],
      () => lets (() => arg, (deviceId) => [deviceId]),
    ),
    // --- other
    () => []
  ),
)

export const generateSecret = (drup, mock) => (ctrlNum, authNum, deviceId) => {
  return tryCatch (
    (secret) => [null, secret],
    (e) => [e, null],
    () => mock | foldMockGenerateSecret (
      // --- no mock
      () => drup.generateSecret (ctrlNum, authNum, deviceId),
      // --- random or specific
      (mockChances, arg) => mockGenerateSecret (mockChances) (
        () => die ('[mock] I/O error during generateSecret:', arg),
        () => lets (() => arg, (secret) => secret),
      ),
      // --- other
      () => {},
    ),
  )
}

export const authenticate = (drup, mock) => (ctrlNum, authNum, deviceId, secret) => {
  return tryCatch (
    (authenticated) => [null, authenticated],
    (e) => [e, null],
    () => mock | foldMockAuthenticate (
      // --- no mock
      () => drup.authenticate (ctrlNum, authNum, deviceId, secret),
      // --- random
      (mockChances, arg) => mockAuthenticate (mockChances) (
        () => die ('[mock] I/O error during authenticate'),
        () => lets (() => arg, (granted) => granted),
      ),
      // --- other
      () => {},
    ),
  )
}
