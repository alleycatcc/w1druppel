export const base64encode = (xs) => Buffer.from (xs).toString ('base64')
export const base64decodeAsU8Array = (x) => new Uint8Array (Buffer.from (x, 'base64'))
export const base64decodeAsString = (x) => Buffer.from (x, 'base64').toString ('ascii')

const bullet = '\u2299'

const getSpinner = ({ speed=1, }) => ({
  cnt: -1,
  len: 6,
  spins: [
    bullet + '   ',
    ' ' + bullet + '  ',
    '  ' + bullet + ' ',
    '   ' + bullet,
    '  ' + bullet + ' ',
    ' ' + bullet + '  ',
  ],
  tick () {
    const { len, cnt, } = this
    this.cnt = (cnt + speed) % 101
    if (this.cnt < 17) return 0
    if (this.cnt < 34) return 1
    if (this.cnt < 51) return 2
    if (this.cnt < 68) return 3
    if (this.cnt < 84) return 4
    else return 5
  },
  spin () {
    const x = this.spins [this.tick ()]
    process.stdout.write ('\r' + x)
  },
})

export const getPrompt = ({ speed=1, } = {}) => {
  const spinner = getSpinner ({ speed, })
  return () => {
    spinner.spin ()
    process.stdout.write (' | ')
  }
}

export const info = (...args) => console.log (
  '٭', ...args,
)

export const error = (...args) => console.error (
  '!', ...args,
)

export const bench = {
  _data: {},
  bench (x) {
    this._data [x] = new Date
  },
  clear () {
    this._data = {}
  },
  get (x) {
    return this._data [x]
  },
}
