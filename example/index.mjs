#!/usr/bin/env node

import { init, } from '../js/lib/index.mjs'

import {
  bench, getPrompt, info, error,
} from './util.mjs'

const BENCH = false

const INTERVAL = {
  default: 100,
  // interacted: 1000,
  interacted: 100,
}

const prompt = getPrompt ({ speed: 3, })
const nil = (x) => x === undefined || x === null

  // bench.bench ('c')
  // info ('[bus ' + String (busNumber) + '] cha-ching')
  // if (BENCH) {
    // info ('[search] ' + String (bench.get ('b') - bench.get ('a')) + ' ms')
    // info ('[authenticate] ' + String (bench.get ('c') - bench.get ('b')) + ' ms')
  // }

const druppelConfig = {
  controllers: [
    {
      tag: 'main',
      config: ['ds2482-800', '/dev/i2c-1', 0x18],
    },
  ],
  authenticators: ['ds1961', 'ds1964'],
  verbose: false,
}

const secrets = {}

const getSpeak = (mock) => ({ ctrlNum, ctrlTag, busNum, authNum, }) => {
  const m = mock ? ' [mock]' : ''
  const s = `[ctrl ${ctrlNum} “${ctrlTag}” / bus ${busNum} / auth ${authNum}]`
  return {
    info: (...i) => info (s + m, ...i),
    error: (...e) => error (s + m, ...e),
  }
}

const sigint = (drup) => {
  process.on ('SIGINT', () => {
    info ('\nsigint, quitting.')
    drup.cleanup ()
    process.exit (0)
  })
}

const go = (mock=false) => {
  const mockBool = Boolean (mock)
  const drup = init (druppelConfig, { mock: mockBool, })

  let cancelPrompt = false
  const doPrompt = () => {
    prompt ()
    if (cancelPrompt) return
    setTimeout (doPrompt, 30)
  }
  doPrompt ()

  sigint (drup)

  const theGetSpeak = getSpeak (mockBool)

  const loop = () => {
    let interacted = false
    let interactedSpeak
    const handlers = {
      onError: (e, info) => {
        theGetSpeak (info).error (e)
      },
      onShort: (info) => {
        theGetSpeak (info).info ('short')
      },
      onPresence: (info) => {
        theGetSpeak (info).info ('presence')
        return true
      },
      onResult: (deviceId, info) => {
        theGetSpeak (info).info ('search discovered device', deviceId)
        return true
      },
      shouldGenerateSecret: (deviceId, _info) => nil (secrets [deviceId]),
      onGenerateSecret: (deviceId, secret, info) => {
        const speak = theGetSpeak (info)
        speak.info ('got secret', secret)
        secrets [deviceId] = secret
        interacted = true
        interactedSpeak = speak
        // --- delete the secret after a while so we can keep testing
        // generateSecret as well
        if (mockBool) setTimeout (() => delete secrets [deviceId], 5000)
      },
      shouldAuthenticate: (deviceId, _info) => secrets [deviceId] || null,
      onAuthenticate: (_deviceId, granted, info) => {
        const speak = theGetSpeak (info)
        if (granted) speak.info ('٭٭٭ [cha-ching] ٭٭٭')
        else speak.info ('٭٭٭ [nope] ٭٭٭')
        interacted = true
        interactedSpeak = speak
      },
    }

    const owConfig = {
      overdrive: 'auto',
      activePullup: 'auto-old',
      networkType: 'single-drop',
    }

    drup.once (owConfig, handlers, mock)

    /* or:
     *   const handle = drup.forever (50, handlers)
     *   ... after some time ...
     *   handle.cancel ()
     */

    if (interacted) {
      console.log ('')
      interactedSpeak.info ('pausing')
    }
    const [f, ms] = interacted ? [
      () => {
        console.log ('')
        interactedSpeak.info ('resuming')
        loop ()
      },
      INTERVAL.interacted,
    ] : [loop, INTERVAL.default]
    setTimeout (f, ms)
  }
  loop ()
}

const { argv: [_, __, opt], argv, } = process
if (opt === '-m') go ('test-probe-authenticate-fails')
// if (opt === '-m') go ('random-fastest')
else if (opt === undefined) go ()
else console.error (`Usage: ${argv [1]} [-m to mock]`)
